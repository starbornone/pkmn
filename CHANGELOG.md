## [Unreleased]

### [0.1.0] - 2020-08-24

### Added
- A generic method of generating Pokemon (`generatePokemon`), using the base Pokemon information and any other provided data, whether they are already partially premade or not. This reduces the amount of repeated code as there will be quite a few instances where Pokemon will have to be generated in various different ways.
- A generic method of calculating remaining Pokemonn (`calculateRemainingPokemon`) for the player and NPCs. This takes a party of Pokemon and returns a number which signifies how many Pokemon within that party are still capable of battling.
- Two separate actions for calculating the player's and NPC opponent's remaining Pokemon which send the player's or opponent's party data to the generic method for doing so. The resulting number is then saved in state.
- A victory check for use during battles (`victoryCheck`) which uses a combination of the remaining Pokemon checks and its own currently active Pokemon health check. This allows the battle state to be changed to one where the player needs to send out another Pokemon, the opponent needs to send out another Pokemon, the opponent has won the match, the player has won the match, or no change to the battle state is made.
- The base details for the following moves: Leech Seed, rapid Spin, Scratch, Smokescreen, Tail Whip, and Withdraw.
- A set of Pokemon states for handling evolution, move learning, and levelling up screens.
- A basic method of granting experience points to a Pokemon (`pokemonGainExperience`). This increases a Pokemon's current experience by the amount provided, then determines whether or not the Pokemon has levelled up, dispatching actions to do so if necessary.
- A basic method of levelling up Pokemon (`pokemonLevelUp`). This levels up the given Pokemon to the level provided after checking whether or not they are able to evolve, in which case, the evolution  process is run before returning here. It then checks to see if they are able to learn any moves, dispatching actions to do so if necessary.
- Eevee to the base Pokemon data list, including its sprite and a list of its evolutions with their requirements.
- The following evolution requirement types (`evolutionRequirements`) to the fake database: `FRIENDSHIP`, `ITEM`, `MOVE`, and `TIME`. These will be used to check whether a Pokemon can evolve or not.
- A concept of `time` to the fake database, including `day` and `night`, each with their own range of applicable hours.
- A collection of miscellanous items (`miscItems`) to the fake database, including Fire Stone, Ice Stone, Leaf Stone, Thunder Stone, and Water Stone.

### Changed
- Moved overall base game states to the fake database. The currently available base states are now `BATTLESTATE`, `POKEMONSTATE`, `STORYSTATE`, and `LOCATIONSTATE`.
- Moved battle phases (`battlePhases`) to a file under battle in the fake database.
- Each Pokemon's set of moves now includes a level which allows the moves to be assigned to new Pokemon or a Pokemon upon level up, based on whether the Pokemon is of high enough level to learn that move.
- The three starter Pokemon now have their correct set of moves, with levels, lined up until level 9.
- Rival's initial set up now selects a Pokemon based on the player's chosen starter - Squirtle if Bulbasaur, Bulbasaur if Charmander, and Charmander if Squirtle.
- Renamed the list of base Pokemon data from `pokemon` to `basePokemon` to avoid confusion.
- Each generated Pokemon now has a friendshp level (`friendship`) and experience values on it - both their current amount of experience (`currentXP`) and the amount of experienced required to reach the next level (`requiredXP`).
- Starter Pokemon are now entirely pregenerated and no longer need to be completed when being added to the party (in `addStarterToParty`).
- Added some miscellaneous items (evolution stones) to the player's starting inventory.
- Items which cannot be used in battle will no longer show their usage buttons during battle, and items which cannot be used outside of battle will no longer show their usage buttons outside of battle.

### Deprecated

### Removed

### Fixed
