# pkmn

Features:

- Bug catching contest
- Lottery in Cerulean and Goldenrod
- Day night cycle affecting types of Pokemon and trainer battles available
- Pokemon trainers in a route or area changing by day, with some persisting and travelling to new areas
- Pokemon Safari
- Pokemon swarms
- Make your own Pokeballs from apricorns
- Battle Frontier with randomly generated trainers, challenge runs with preset Pokemon teams, and weekly tournaments

Upcoming TODO:

- Conversation between starter Pokemon after character creation.
- Levelling up a Pokemon. This will include them learning new moves.
- An experience check when experience is gained to see if a Pokemon needs to go into the levelling up phase.
- A level check when a level is gained to see if a Pokemon should evolve.
- Basic healing item usage (potion) in and out of battle. We'll be reducing the number of redundant or useless money sink items, so there probably won't be super and max potions. Perhaps levelling up the potion (number of potions and their healing amount) and letting them be refilled for free at the Poke Centre is a more elegant solution?
- Status effects.
- Catching a Pokemon with a Pokeball.