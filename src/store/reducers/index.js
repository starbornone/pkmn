import { combineReducers } from 'redux';

import auth from 'auth/store/reducers'; // TODO:

import loadingReducer from 'store/reducers/general/loading.reducer';
import messageReducer from 'store/reducers/general/message.reducer';

import characterReducer from 'store/reducers/character.reducer';
import inventoryReducer from 'store/reducers/inventory.reducer';

import npcReducer from 'store/reducers/npc.reducer';
import partyReducer from 'store/reducers/party.reducer';

import battleReducer from 'store/reducers/battle.reducer';
import locationReducer from 'store/reducers/location.reducer';
import storyReducer from 'store/reducers/story.reducer';

const appReducer = combineReducers({
  loading: loadingReducer,
  message: messageReducer,
  battle: battleReducer,
  character: characterReducer,
  inventory: inventoryReducer,
  location: locationReducer,
  npc: npcReducer,
  party: partyReducer,
  story: storyReducer
})

const rootReducer = (state, action) => {
  return appReducer(state, action);
}

export default rootReducer;