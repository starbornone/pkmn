import * as Actions from 'store/actions';
import { storyStates } from 'database';

const initialState = {
  hasStarted: false,
  currentState: storyStates.titlescreen
}

const story = (state = initialState, action) => {
  switch (action.type) {
    case Actions.STORY_CHANGE_STATE: {
      return {
        ...state,
        currentState: action.payload
      };
    }
    case Actions.STORY_START_TOGGLE: {
      return {
        ...state,
        hasStarted: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default story;