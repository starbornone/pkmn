import * as Actions from 'store/actions';

import { locationStates } from 'database';

const initialState = {
  currentLocation: locationStates.pallettown
}

const location = (state = initialState, action) => {
  switch (action.type) {
    default: {
      return state;
    }
  }
};

export default location;