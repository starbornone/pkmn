import * as Actions from 'store/actions';

const initialState = {
  name: ''
}

const character = (state = initialState, action) => {
  switch (action.type) {
    case Actions.CHARACTER_CREATE: {
      return {
        ...state,
        ...action.payload
      }
    }
    default: {
      return state;
    }
  }
};

export default character;