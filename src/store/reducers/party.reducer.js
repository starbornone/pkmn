import * as Actions from 'store/actions';
import * as Helpers from 'store/reducers/helpers';

const initialState = {
  activePokemon: 0,
  party: [],
  remainingPokemon: 0
}

const party = (state = initialState, action) => {
  switch (action.type) {
    case Actions.PARTY_ADD_POKEMON: {
      return {
        ...state,
        party: [...state.party, action.payload]
      }
    }
    case Actions.PARTY_CALCULATE_REMAINING_POKEMON: {
      return {
        ...state,
        remainingPokemon: action.payload
      }
    }
    case Actions.PARTY_MODIFY_POKEMON: {
      return {
        ...state,
        party: Helpers.updateArrayObject(state.party, action.payload)
      }
    }
    case Actions.PARTY_SET_ACTIVE_POKEMON: {
      return {
        ...state,
        activePokemon: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default party;