import * as Actions from 'store/actions';
import * as Helpers from 'store/reducers/helpers';

import { battlePhases } from 'database';

const initialState = {
  battlePhase: battlePhases.PLAYER_PICKING_TURN,
  log: [],
  turn: 0
}

const battle = (state = initialState, action) => {
  switch (action.type) {
    case Actions.BATTLE_LOG_ADD: {
      return {
        ...state,
        log: [...state.log, action.payload]
      }
    }
    case Actions.BATTLE_SET_PHASE: {
      return {
        ...state,
        battlePhase: action.payload
      }
    }
    case Actions.BATTLE_SET_TURN: {
      return {
        ...state,
        turn: action.payload
      }
    }
    default: {
      return state;
    }
  }
};

export default battle;