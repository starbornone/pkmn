import * as Actions from 'store/actions';
import * as Helpers from 'store/reducers/helpers';

const initialState = {
  opponent: {
    name: 'Green',
    activePokemon: 0,
    hasActed: false,
    party: [],
    remainingPokemon: 0
  }
}

const npcs = (state = initialState, action) => {
  switch (action.type) {
    case Actions.NPC_ADD_POKEMON: {
      return {
        ...state,
        opponent: {
          ...state.opponent,
          party: [...state.opponent.party, action.payload]
        }
      };
    }
    case Actions.NPC_CALCULATE_REMAINING_POKEMON: {
      return {
        ...state,
        opponent: {
          ...state.opponent,
          remainingPokemon: action.payload
        }
      };
    }
    case Actions.NPC_HAS_ACTED_TOGGLE: {
      return {
        ...state,
        opponent: {
          ...state.opponent,
          hasActed: action.payload
        }
      };
    }
    case Actions.NPC_MODIFY_POKEMON: {
      return {
        ...state,
        opponent: {
          ...state.opponent,
          party: Helpers.updateArrayObject(state.opponent.party, action.payload)
        }
      };
    }
    case Actions.NPC_SET_HAS_ACTED: {
      return {
        ...state,
        opponent: {
          ...state.opponent,
          hasActed: action.payload
        }
      };
    }
    case Actions.NPC_SET_ACTIVE_POKEMON: {
      return {
        ...state,
        opponent: {
          ...state.opponent,
          activePokemon: action.payload
        }
      };
    }
    default: {
      return state;
    }
  }
};

export default npcs;