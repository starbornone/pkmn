import _ from '@lodash';
import * as Actions from 'store/actions';

import { medicine, miscItems } from 'database';

// TODO: Set up the player's starting items elsewhere.
const initialState = {
  battleItems: [],
  berries: [],
  keyItems: [],
  machines: [],
  medicines: _.map(medicine, (med => {
    return {
      ...med,
      amount: 10
    }
  })),
  miscItems: _.map(miscItems, (misc => {
    return {
      ...misc,
      amount: 1
    }
  })),
  pokeballs: []
};

const inventory = (state = initialState, action) => {
  switch (action.type) {
    case Actions.INVENTORY_TAKE_ITEM: {
      const { section, items } = action.payload;
      return {
        ...state,
        [section]: items
      }
    }
    default: {
      return state;
    }
  }
};

export default inventory;