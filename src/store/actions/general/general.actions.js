export function resetState() {
  localStorage.removeItem('state');
  window.location.reload(false);
}