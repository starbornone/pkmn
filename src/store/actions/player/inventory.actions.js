import _ from '@lodash';

export const INVENTORY_TAKE_ITEM = '[INVENTORY] TAKE ITEM';
export const INVENTORY_CALCULATE_CARRIED_WEIGHT = '[INVENTORY] CALCULATE CARRIED WEIGHT';

export function calculateCarriedWeight() {
  return (dispatch, getState) => {
    const { carried, equipped } = getState().character.inventory;
    const allItems = [
      ...carried.items,
      ...equipped.other,
      ...equipped.weapons,
      ...equipped.worn
    ];
    let carriedWeight = 0;
    allItems.map(item => {
      carriedWeight += item.weight;
    });
    dispatch({
      type: INVENTORY_CALCULATE_CARRIED_WEIGHT, payload: carriedWeight
    });
  }
}

export function takeItems(status, section, items) {
  return dispatch => {
    dispatch({
      type: INVENTORY_TAKE_ITEM, payload: { status, section, items }
    });
    dispatch(calculateCarriedWeight());
  }
}

export function wearItems(currentlyWorn, status, section, items) {
  return dispatch => {
    const newItemSet = items;
    currentlyWorn.map(wornItem => {
      const foundLayer = _.findIndex(newItemSet, { slot: wornItem.slot, layer: wornItem.layer });
      if (foundLayer === -1) {
        newItemSet.push(wornItem);
      }
    });
    dispatch({
      type: INVENTORY_TAKE_ITEM, payload: { status, section, items: newItemSet }
    });
    dispatch(calculateCarriedWeight());
  }
}