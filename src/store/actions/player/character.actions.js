import _ from '@lodash';
import * as Actions from 'store/actions';

import { characterModels, states } from 'database';

export const CHARACTER_CREATE = '[CHARACTER] CREATE';

export function characterCreation({ character }) {
  return dispatch => {
    const model = _.find(characterModels, { fieldValue: character.sprite });
    const characterData = {
      ...character,
      model: model
    }
    dispatch({ type: CHARACTER_CREATE, payload: characterData });
    dispatch(Actions.startToggle(true));
    dispatch(Actions.changeStoryState(states.STORYSTATE, 'starterselect'));
  }
}