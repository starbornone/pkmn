import history from '@history';
import _ from '@lodash';

import * as Actions from 'store/actions';
import { states } from 'database';

export const PARTY_ADD_POKEMON = '[PARTY] ADD POKEMON';
export const PARTY_CALCULATE_REMAINING_POKEMON = '[PARTY] CALCULATE REMAINING POKEMON';
export const PARTY_MODIFY_POKEMON = '[PARTY] MODIFY POKEMON';
export const PARTY_SET_ACTIVE_POKEMON = '[PARTY] SET ACTIVE POKEMON';

export function addStarterToParty(pokemon) {
  return dispatch => {
    dispatch({ type: PARTY_ADD_POKEMON, payload: pokemon });
    dispatch(Actions.changeStoryState(states.BATTLESTATE, 'battletrainer'));
    dispatch(calculateRemainingPartyPokemon());
    history.push('/game');
  }
}

export function addPokemonToParty(id, iv, ev, level) {
  return dispatch => {
    dispatch({ type: PARTY_ADD_POKEMON, payload: {} });
  }
}

export function calculateRemainingPartyPokemon() {
  return (dispatch, getState) => {
    const state = getState();
    const remainingPokemon = Actions.calculateRemainingPokemon(state.party.party);
    dispatch({ type: PARTY_CALCULATE_REMAINING_POKEMON, payload: remainingPokemon });
  }
}