import _ from '@lodash';

import * as Actions from 'store/actions';

/**
 * @name            learnNewMove
 * @description     ...
 * @param {object}  pokemon       The Pokemon that will learn the new move.
 * @param {object}  newMove       The new move.
 */
export function learnNewMove(pokemon, newMove) {
  return (dispatch, getState) => {
    if (pokemon.moves.length >= 4) {
      dispatch(learnNewMovePrompt(pokemon, newMove));
    } else {
      let state = getState();
      const index = _.findIndex(state.party.party, { id: pokemon.id });
      dispatch({
        type: Actions.PARTY_MODIFY_POKEMON, payload: {
          index,
          item: {
            ...pokemon,
            moves: [...pokemon.moves, newMove]
          }
        }
      });
    }
  }
}

/**
 * @name            learnNewMovePrompt
 * @description     ...
 * @param {object}  pokemon       The Pokemon that will learn the new move.
 * @param {object}  newMove       The new move.
 */
export function learnNewMovePrompt(pokemon, newMove) {
  return dispatch => {

  }
}

/**
 * @name            learnNewMoveResult
 * @description     ...
 * @param {object}  pokemon       The Pokemon that will learn the new move.
 * @param {object}  newMove       The new move.
 */
export function learnNewMoveResult(pokemon, newMove) {
  return dispatch => {

  }
}