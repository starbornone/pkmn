import _ from '@lodash';

import * as Actions from 'store/actions';

import { hashFnv32a } from 'utilities';

/**
 * @name            calculateHP
 */
export function calculateHP(baseStat, iv, ev, level) {
  return Math.trunc((((2 * baseStat + iv + (ev / 4)) * level) / 100) + level + 10);
}

/**
 * @name            calculateRemainingPokemon
 * @description     Takes a party of Pokemon and returns a number which signifies how
 *                  many Pokemon within that party are still capable of battling.
 * @param {array}  party        The collection of Pokemon that will be used for counting
 *                              the remaining battle-capable ones.
 */
export function calculateRemainingPokemon(party) {
  let remainingPokemon = 0;
  party.map(pkmn => {
    if (pkmn.health.currentHP > 0) {
      remainingPokemon++;
    }
  });
  return remainingPokemon;
}

/**
 * @name            calculateStat
 */
export function calculateStat(baseStat, iv, ev, level) {
  return Math.trunc((((2 * baseStat + iv + (ev / 4)) * level) / 100) + 5);
}

/**
 * @name            generatePokemon
 * @description     Generates a Pokemon using the base Pokemon information and any other
 *                  provided data.
 * @param {object}  pokemon       The base Pokemon data or partially complete Pokemon.
 * @param {string}  team          What kind of owner this Pokemon has.
 * @param {integer} level         The intended level of the Pokemon.
 * @param {integer} iv            The Pokemon's Individual Values.
 * @param {integer} ev            The Pokemon's Effort Values.
 */
export function generatePokemon(pokemon, level, iv, ev) {
  let stats = {};
  // If the Pokemon is being generated from scratch, figure out their stats. Otherwise
  // use those which come with the Pokemon.
  if (iv && ev) {
    stats = {
      hp: calculateHP(pokemon.baseStats.hp, iv, ev, level),
      attack: calculateStat(pokemon.baseStats.attack, iv, ev, level),
      defense: calculateStat(pokemon.baseStats.defense, iv, ev, level),
      spatk: calculateStat(pokemon.baseStats.spatk, iv, ev, level),
      spdef: calculateStat(pokemon.baseStats.spdef, iv, ev, level),
      speed: calculateStat(pokemon.baseStats.speed, iv, ev, level)
    }
  }

  const moves = pokemon.moves.map(move => {
    // Only add the move if the Pokemon is high enough level for it.
    if (level && level >= move.level) {
      return {
        ...move,
        currentPP: move.pp
      }
    }
  });

  let newPkmn = {
    ...pokemon,
    id: hashFnv32a(pokemon.id + '-' + pokemon.name),
    pokedexId: pokemon.id,
    moves: moves.filter(item => item),
    health: {
      currentHP: stats.hp ? stats.hp : pokemon.stats.hp,
      maxHP: stats.hp ? stats.hp : pokemon.stats.hp
    },
    stats: !_.isEmpty(stats) ? stats : pokemon.stats,
    status: 'healthy',
    experience: {
      currentXP: level ? Math.pow(level, 3) : Math.pow(pokemon.level, 3),
      requiredXp: level ? Math.pow(level + 1, 3) : Math.pow(pokemon.level + 1, 3)
    },
    friendship: 0
  }
  // Make sure that the Pokemon only has four moves. Currently, we're just taking their 4
  // newest moves. However, later we should add some logic so that they pick "good" moves.
  if (newPkmn.moves.length > 4) {
    newPkmn.moves = newPkmn.moves.slice(newPkmn.moves.length - 4, newPkmn.moves.length);
  }
  return newPkmn;
}

/**
 * @name            reduceMovePP
 */
export function reduceMovePP(pokemon, move) {
  return (dispatch, getState) => {
    let state = getState();
    const index = _.findIndex(state.party.party, { id: pokemon.id });

    const usedMoveIndex = _.findIndex(state.party.party[index].moves, { name: move.name });
    let modifiedMoves = pokemon.moves;
    modifiedMoves[usedMoveIndex].currentPP = modifiedMoves[usedMoveIndex].currentPP - 1;

    dispatch({
      type: Actions.PARTY_MODIFY_POKEMON, payload: {
        index,
        item: {
          ...pokemon,
          moves: modifiedMoves
        }
      }
    });
  }
}

/**
 * @name            replenishHP
 */
export function replenishHP(team, pokemon, amount) {
  return (dispatch, getState) => {
    let state = getState();
    const index = _.findIndex(state.party.party, { id: pokemon.id });

    let modifiedTarget = pokemon;
    if (amount) {
      modifiedTarget.health.currentHP = modifiedTarget.health.currentHP + amount;
    } else {
      modifiedTarget.health.currentHP = modifiedTarget.health.maxHP;
    }
    if (modifiedTarget.health.currentHP > modifiedTarget.health.maxHP) {
      modifiedTarget.health.currentHP = modifiedTarget.health.maxHP;
    }

    const dispatchType = team === 'PARTY' ? Actions.PARTY_MODIFY_POKEMON : Actions.NPC_MODIFY_POKEMON;
    dispatch({
      type: dispatchType, payload: {
        index,
        item: modifiedTarget
      }
    });
  }
}

/**
 * @name            replenishPP
 */
export function replenishPP(team, pokemon, move, amount) {
  return (dispatch, getState) => {
    let state = getState();
    const index = _.findIndex(state.party.party, { id: pokemon.id });

    let modifiedTarget = {
      ...pokemon,
      moves: pokemon.moves.map(existingMove => {
        let newPPAmount = 0;

        if (move) {
          if (move.name === existingMove.name) {
            newPPAmount = amount ? existingMove.currentPP + amount : existingMove.pp;
          } else {
            newPPAmount = existingMove.currentPP;
          }
          if (newPPAmount > existingMove.pp) {
            newPPAmount = existingMove.pp;
          }
        } else {
          newPPAmount = existingMove.pp;
        }

        return {
          ...existingMove,
          currentPP: newPPAmount
        }
      })
    };

    const dispatchType = team === 'PARTY' ? Actions.PARTY_MODIFY_POKEMON : Actions.NPC_MODIFY_POKEMON;
    dispatch({
      type: dispatchType, payload: {
        index,
        item: modifiedTarget
      }
    });
  }
}