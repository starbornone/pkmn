import _ from '@lodash';

import * as Actions from 'store/actions';

import { basePokemon, states } from 'database';

/**
 * @name            pokemonGainExperience
 * @description     Increase a Pokemon's current experience by the amount provided, then
 *                  determine whether or not the Pokemon has levelled up, dispatching
 *                  actions to do so if necessary.
 * @param {object}  pokemon       The Pokemon to gain experience.
 * @param {integer} experience    The amount of experience.
 */
export function pokemonGainExperience(pokemon, experience) {
  return (dispatch, getState) => {
    let state = getState();
    const index = _.findIndex(state.party.party, { id: pokemon.id });

    // First, we add the new experience to the current amount of experience that the
    // Pokemon has.
    let newXP = pokemon.experience.currentXP + experience;
    let modifiedPkmn = {};

    // Check if the current amount of experience now exceeds the amount of experience required to level up.
    if (newXP > pokemon.experience.requiredXP) {

      // If the Pokemon has enough experience to level up...
      // We calculated how much experience will be required for the next level.
      const requiredXP = Math.pow(pokemon.level + 1, 3);
      // Then we modify the Pokemon to have the new required experience amount and any
      // excess from their newly gained experience gets added to their current expeirence.
      // For example, say a Pokemon has 50 experience and needs 100 experience to level
      // up. This Pokemon then gains 100 experience. They will level up, so their new
      // current experience is 50, because 50 of their gained experience went towards the
      // new level and the remaining 50 became their current experience.
      modifiedPkmn = {
        ...pokemon,
        experience: {
          currentXP: newXP - pokemon.experience.requiredXP,
          requiredXP
        }
      };
      // After creating the modified Pokemon, we run the action to change the game state
      // into one where the Pokemon will get levelled up.
      dispatch(Actions.changeStoryState(states.POKEMONSTATE, 'levelup'));
      // We also run the action which levels up the Pokemon, passing the Pokemon itself
      // to it and its new level.
      dispatch(pokemonLevelUp(pokemon, pokemon.level + 1));
    } else {

      // If the Pokemon hasn't gained enough experience to level up, we just change their
      // current experience to the new amount.
      modifiedPkmn = {
        ...pokemon,
        experience: {
          ...pokemon.experience,
          currentXP: newXP
        }
      };
    }

    dispatch({
      type: Actions.PARTY_MODIFY_POKEMON, payload: {
        index,
        item: modifiedPkmn
      }
    });
  }
}

/**
 * @name            pokemonLevelUp
 * @description     Level up the given Pokemon to the level provided after checking
 *                  whether or not they are able to evolve, in which case, the evolution
 *                  process is run before returning here. It then checks to see if they
 *                  are able to learn any moves, dispatching actions to do so if
 *                  necessary.
 * @param {object}  pokemon       The Pokemon to be levelled up.
 * @param {integer} level         The new level of the Pokemon.
 */
export function pokemonLevelUp(pokemon, level) {
  return (dispatch, getState) => {
    // Check if there are any evolutions for this Pokemon before we do anything else. This is because we want to use the new base stats of the Pokemon if they evolve into another form. We also want them to gain any moves from their evolved form for this level, if there are any.
    const canEvolve = Actions.pokemonCheckEvolution(pokemon, level);

    if (canEvolve) {
      // If there is an evolution for the Pokemon's level, go into the evolution state. This can be cancelled, so we won't do any further things within this level up action.
      // Once the evolution state has been resolved, pokemonLevelUp will be called again and, because the provided Pokemon will be its evolved form, the canEvolve check will return `false` the next time through. This is because the evolved form will not be of a high enough level to evolve yet again.
      dispatch(Actions.changeStoryState(states.POKEMONSTATE, 'evolution'));
    } else {

      // If the Pokemon cannot evolve, we continue with levelling them up. For this, we need their baseStats, ev, and iv.
      const { baseStats, ev, iv } = pokemon;

      // Then we generate the Pokemon's stats from scratch using the new level. This means
      // that if their IVs or EVs have increased, they retroactively benefit from the
      // increase. (Although, this would mean that an IV or EV increase won't affect a max
      // level Pokemon.)
      const stats = {
        hp: Actions.calculateHP(baseStats.hp, iv, ev, level),
        attack: Actions.calculateStat(baseStats.attack, iv, ev, level),
        defense: Actions.calculateStat(baseStats.defense, iv, ev, level),
        spatk: Actions.calculateStat(baseStats.spatk, iv, ev, level),
        spdef: Actions.calculateStat(baseStats.spdef, iv, ev, level),
        speed: Actions.calculateStat(baseStats.speed, iv, ev, level)
      };

      // When we modify the Pokemon to have the new level and stats, we also need to make
      // sure that their health is increased accordingly.
      let modifiedPkmn = {
        ...pokemon,
        level,
        stats,
        health: {
          ...pokemon.health,
          maxHP: stats.hp
        }
      };

      // Then we send the update for the Pokemon to have these new values.
      let state = getState();
      const index = _.findIndex(state.party.party, { id: pokemon.id });
      dispatch({
        type: Actions.PARTY_MODIFY_POKEMON, payload: {
          index,
          item: modifiedPkmn
        }
      });

      // Now that the Pokemon has levelled, we find the base data for the current Pokemon
      // to see if they can learn any new moves.
      const base = _.find(basePokemon, { id: pokemon.pokedexId });
      const newMove = _.find(base.moves, { level });
      if (!_.isEmpty(newMove)) {
        
        // If there is a new move available for the Pokemon, set the Pokemon state to
        // learnmove.
        dispatch(Actions.changeStoryState(states.POKEMONSTATE, 'learnmove'));
      }
    }
  }
}