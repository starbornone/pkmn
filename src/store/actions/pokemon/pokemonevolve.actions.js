import _ from '@lodash';

import * as Actions from 'store/actions';
import { basePokemon, evolutionRequirements } from 'database';

/**
 * @name            pokemonCheckEvolution
 * @description     Check whether a Pokemon can evolve. This check is for evolution based
 *                  on level. For evolutions triggered by other requirements, such as
 *                  item use, that check will be done on the use of the item itself.
 * @param {object}  pokemon       The Pokemon to be checked for evolution.
 * @param {integer} level         The level that the Pokemon will become.
 */
export function pokemonCheckEvolution(pokemon, level) {
  // Find the base data for the current Pokemon to see if they can evolve.
  const base = _.find(basePokemon, { id: pokemon.pokedexId });

  let canEvolve = false;
  let continueChecks = true;

  // We use `continueChecks` to control when we stop checking requirements.
  while (continueChecks) {

    // Check if there are any evolutions for this Pokemon. This comes before we handle their stats for levelling up because we want to use the new base stats of the Pokemon if they evolve into another form. We also want them to gain any moves from their evolved form for this level, if there are any.
    if (!_.isEmpty(base.evolutions)) {

      // See if any of their evolutions can happen at their new level, creating a list of such possible evolutions.
      // NOTE: A level of 0 means that there's no level requirement for this Pokemon to evolve, but their evolution still happens on level up.
      const possibleEvolutions = _.filter(base.evolutions, (evolution) => {
        return evolution.level && (evolution.level === level || evolution.level === 0)
      });

      if (!_.isEmpty(possibleEvolutions)) {
        // If the list of possible evolutions is not empty, we go through their
        // requirements and compare them to the current state of the Pokemon.
        possibleEvolutions.map(evolution => {
          if (!_.isEmpty(evolution.otherRequirements)) {
            evolution.otherRequirements.map(requirement => {

              // The types of requirements currently checked here are friendship level,
              // whether they have a move of a certain type, and the time of day. If the
              // requirement is met, we change `canEvolve` to true, to signify that they
              // meet this requirement.
              // However, if any of the checks fail, we not only set `canEvolve` to false,
              // we also set `continueChecks` to false. Because of this, all requirements
              // must be met for evolution to occur.
              switch (requirement.type) {

                case evolutionRequirements.FRIENDSHIP: {
                  // Friendship level check. Checks whether or not the Pokemon's current
                  // friendship level is high enough.
                  if (pokemon.friendship >= requirement.value) {
                    canEvolve = true;
                  } else {
                    canEvolve = false;
                    continueChecks = false;
                  }
                  break;
                }

                case evolutionRequirements.MOVE: {
                  // Move type check. Checks whether or not a move exists within their
                  // current move set that matches the provided type.
                  const moveCheck = _.find(pokemon.moves, { type: requirement.value });
                  if (!_.isEmpty(moveCheck)) {
                    canEvolve = true;
                  } else {
                    canEvolve = false;
                    continueChecks = false;
                  }
                  break;
                }

                case evolutionRequirements.TIME: {
                  // Time of day check
                  // TODO:
                  break;
                }

                default: {
                  break;
                }
              }

            })
          } else {
            // If there are no other requirements, but the level requirement has been
            // met, we set canEvolve to true and complete this process.
            canEvolve = true;
            continueChecks = false;
          }

        })
      } else {
        // If there are no possible evolutions at this stage, we complete this process and
        // `canEvolve` stays false.
        continueChecks = false;
      }

    } else {
      // If there are no possible evolutions at all, we complete this process and
      // `canEvolve` stays false.
      continueChecks = false;
    }

  }

  // Return whether or not this Pokemon can now evolve.
  return canEvolve;
}

/**
 * @name            pokemonEvolvePrompt
 * @description     ...
 * @param {object}  pokemon       The Pokemon to evolve.
 */
export function pokemonEvolvePrompt(pokemon) {
  return dispatch => {
  }
}

/**
 * @name            pokemonEvolveResult
 * @description     ...
 * @param {object}  pokemon       The Pokemon to evolve.
 */
export function pokemonEvolveResult(pokemon) {
  return dispatch => {
  }
}