import * as Actions from 'store/actions';
import { starters } from 'database';

export const NPC_ADD_POKEMON = '[NPC] ADD POKEMON';
export const NPC_CALCULATE_REMAINING_POKEMON = '[NPC] CALCULATE REMAINING POKEMON';

export function calculateRemainingNPCPokemon() {
  return (dispatch, getState) => {
    const state = getState();
    let remainingPokemon = Actions.calculateRemainingPokemon(state.npc.opponent.party);
    dispatch({ type: NPC_CALCULATE_REMAINING_POKEMON, payload: remainingPokemon });
  }
}

export function setupRival(playerStarter) {
  return dispatch => {
    let mainPkmn = {};
    if (playerStarter.name === 'Bulbasaur') {
      // The rival starts with Squirtle.
      mainPkmn = Actions.generatePokemon(starters[2]);
    } else if (playerStarter.name === 'Charmander') {
      // The rival starts with Bulbasaur.
      mainPkmn = Actions.generatePokemon(starters[0]);
    } else if (playerStarter.name === 'Squirtle') {
      // The rival starts with Charmander.
      mainPkmn = Actions.generatePokemon(starters[1]);
    }
    dispatch({ type: NPC_ADD_POKEMON, payload: mainPkmn });
    dispatch(calculateRemainingNPCPokemon());
  }
}