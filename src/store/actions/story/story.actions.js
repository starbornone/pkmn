import _ from '@lodash';

import { battleStates, locationStates, pokemonStates, storyStates, states } from 'database';

export const STORY_START_TOGGLE = '[STORY] START TOGGLE';
export const STORY_CHANGE_STATE = '[STORY] CHANGE STATE';

/**
 * @name            changeStoryState
 * @description     ...
 * @param {string}  newStateType       ...
 * @param {string}  newStateName       ...
 */
export function changeStoryState(newStateType, newStateName) {
  return dispatch => {
    let newState = {};
    switch (newStateType) {
      case states.BATTLESTATE: {
        newState = battleStates[newStateName];
        break;
      }
      case states.POKEMONSTATE: {
        newState = pokemonStates[newStateName];
        break;
      }
      case states.STORYSTATE: {
        newState = storyStates[newStateName];
        break;
      }
      case states.LOCATIONSTATE: {
        // TODO: use location reducer instead
        newState = locationStates[newStateName];
        break;
      }
    }
    dispatch({ type: STORY_CHANGE_STATE, payload: newState });
  }
}

/**
 * @name            startToggle
 */
export function startToggle() {
  return dispatch => {
    dispatch({ type: STORY_START_TOGGLE });
  }
}