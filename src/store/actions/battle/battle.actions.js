import _ from '@lodash';
import * as Actions from 'store/actions';

import { battlePhases, effects, targets } from 'database';

export const BATTLE_LOG_ADD = '[BATTLE] LOG ADD';
export const BATTLE_SET_PHASE = '[BATTLE] SET PHASE';
export const BATTLE_SET_TURN = '[BATTLE] SET TURN';

export function manageMoveEffect(turn, move, effect, user, target) {
  return dispatch => {
    switch (effect.type) {
      case effects.DAMAGE: {
        dispatch(Actions.dealDamage(turn, move, user, target));
        break;
      }
      default: {
        break;
      }
    }
  }
}

export function manageStageEffect() {
  return dispatch => {
  }
}

/**
 * @name            manageMove
 * @description     The first stage of handling a Pokemon's move during battle.
 * @param {integer} turn              The current battle's turn number.
 * @param {object}  move              The entire move object.
 * @param {object}  userPokemon       The entire object of the Pokemon that used the move.
 */
export function manageMove(turn, move, userPokemon) {
  return dispatch => {
    console.log('>>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> ');
    // If the move has an accuracy of 100 or 0, we consider it as a hit.
    const hitSuccess = move.accuracy === 100 || move.accuracy === 0 ?
      true :
      // Otherwise, we roll a random number between 0 and 99. If the resulting number is
      // less than or equal to the move's accuracy, we consider it to be a "hit".
      move.accuracy <= Math.floor(Math.random() * 100) ?
        true :
        // If the final number is greater than the accuracy of the move, it is a miss.
        false;

    if (hitSuccess) {
      // Handle the case where the move "hits".
      // This does not necessarily mean that it deals damage, successfuly applies an
      // effect, etc., but rather that we continue to run our other checks.
      dispatch(manageMoveTarget(turn, move, userPokemon));
    } else {
      // Handle the case where the move misses entirely.
      // We add the miss to the battle log.
      dispatch({
        type: Actions.BATTLE_LOG_ADD, payload: {
          turn,
          text: `${userPokemon.name} tried to use ${move.name} but missed!`
        }
      })
    }

    if (userPokemon.trainer && userPokemon.trainer === 'player') {
      // Regardless of "hit", we reduce the user's PP.
      dispatch(Actions.reduceMovePP(userPokemon, move));
    }
    console.log('>>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> >>> ');
  }
}

export function manageMoveTarget(turn, move, userPokemon) {
  return (dispatch, getState) => {
    let state = getState();

    const opposingParty = userPokemon.trainer === 'player' ? state.npc.opponent.party : state.party.party;

    const team = userPokemon.trainer === 'player' ? 'OPPONENT' : 'PLAYER';

    const activeOpposingPkmn = userPokemon.trainer === 'player' ? state.npc.opponent.activePokemon : state.party.activePokemon;
    console.log('activeOpposingPkmn >>>', activeOpposingPkmn);

    move.effects.map(effect => {
      switch (effect.targets) {
        case targets.ALL_ACTIVE: {
          dispatch(manageMoveEffect(turn, move, effect, userPokemon, { team: targets.ALL_ACTIVE }));
          break;
        }
        case targets.BATTLE_STAGE: {
          dispatch(manageStageEffect(turn, move, effect));
          break;
        }
        case targets.OPPONENT_SINGLE: {
          // TODO: Implement targeting.
          // TODO: Use the active Pokemon as the opponent.
          dispatch(manageMoveEffect(turn, move, effect, userPokemon, {
            team,
            targetPkmn: opposingParty[activeOpposingPkmn]
          }));
          break;
        }
        case targets.OPPONENT_TEAM: {
          dispatch(manageMoveEffect(turn, move, effect, userPokemon, {
            team,
            targetPkmn: opposingParty
          }));
          break;
        }
        case targets.SELF_SINGLE: {
          dispatch(manageMoveEffect(turn, move, effect, userPokemon));
          break;
        }
        case targets.SELF_TEAM: {
          dispatch(manageMoveEffect(turn, move, effect, userPokemon));
          break;
        }
        default: {
          break;
        }
      }
    })
  }
}

export function manageTurn(turn, move, userPokemon, rivalPkmn) {
  return (dispatch, getState) => {
    // TODO: Figure out how to make sure the NPC doesn't attack on the same turn that they switch Pokemon (or use an item). Maybe a stepper could work?
    // The NPC compares the two Pokemon facing each other and determines whether or not they need to switch Pokemon.
    dispatch(Actions.manageNPCBattleSwitch(turn, userPokemon, rivalPkmn));
    // Send off the turn number, move object, and entire Pokemon that used the move for
    // move calculation.
    // TODO: Modify this for a case where there's more than one active Pokemon in the
    // player's team, e.g. double or triple battles.
    dispatch(manageMove(turn, move, userPokemon));
    // TODO: If the player has defeated the opposing Pokemon, the opponent will send out another Pokemon.
    dispatch(victoryCheck(turn, userPokemon, rivalPkmn));
    // TODO: If the opposing trainer has no Pokemon left, the player wins.

    let state = getState();
    const { hasActed } = state.npc.opponent;

    if (!hasActed) {
      dispatch(Actions.manageNPCBattleTurn(turn, userPokemon, rivalPkmn));
      dispatch(victoryCheck(turn, userPokemon, rivalPkmn));
    }
    // TODO: If the player has no Pokemon left, the opponent wins.

    state = getState();
    const { battlePhase } = state.battle;

    console.log('battlePhase >>>', battlePhase);

    // Only do the next steps if no one has won, as this will change the battle phase.
    if (battlePhase !== battlePhases.BATTLE_WON_OPPONENT && battlePhase !== battlePhases.BATTLE_WON_PLAYER) {
      // Increment the turn counter by one once both Pokemon have had their move.
      dispatch(setTurn(turn + 1));
      // Set that the NPC has not acted this turn, so that they may act once the player makes their move.
      dispatch(Actions.setNPCHasActed(false));
      // Set the battle phase to "TURN_RESULTS" so that the results of the turn are displayed via the BattleLog.
      dispatch(setPhase(battlePhases.TURN_RESULTS));
    }
  }
}

/**
 * @name            victoryCheck
 * @description     Using a combination of the remaining Pokemon checks and a currently
 *                  active Pokemon health check for both the player and the NPC opponent,
 *                  the battle state can be changed to one where the player needs to send
 *                  out another Pokemon, the opponent needs to send out another Pokemon,
 *                  the opponent has won the match, the player has won the match. If the
 *                  criteria for these is not met, no change to the battle state is made.
 * @param {integer} turn         ...
 * @param {object}  userPokemon  ...
 * @param {object}  rivalPkmn    ...
 */
export function victoryCheck(turn, userPokemon, rivalPkmn) {
  return (dispatch, getState) => {
    dispatch(Actions.calculateRemainingPartyPokemon());
    dispatch(Actions.calculateRemainingNPCPokemon());

    console.log('VICTORY CHECK >>>', );

    const state = getState();
    const { npc: { opponent }, party } = state;

    if (userPokemon.health.currentHP <= 0 && party.remainingPokemon > 0) {
      console.log('userPokemon.health.currentHP <= 0 && party.remainingPokemon > 0');
      dispatch(setPhase(battlePhases.PLAYER_PICKING_POKEMON));
    } else if (rivalPkmn.health.currentHP <= 0 && opponent.remainingPokemon > 0) {
      console.log('rivalPkmn.health.currentHP <= 0 && opponent.remainingPokemon > 0');
      dispatch(Actions.manageNPCBattleSwitch(turn, userPokemon, rivalPkmn));
    }

    if (party.remainingPokemon <= 0) {
      console.log('party.remainingPokemon <= 0');
      dispatch(setPhase(battlePhases.BATTLE_WON_OPPONENT));
    } else if (opponent.remainingPokemon <= 0) {
      console.log('opponent.remainingPokemon <= 0');
      dispatch(setPhase(battlePhases.BATTLE_WON_PLAYER));
    }
  }
}

export function setActivePokemon(turn, team, party, pkmnId) {
  return dispatch => {
    const dispatchType = team === 'PARTY' ? Actions.PARTY_SET_ACTIVE_POKEMON : Actions.NPC_SET_ACTIVE_POKEMON;

    const newPkmn = _.findIndex(party, { id: pkmnId });
    dispatch({ type: dispatchType, payload: newPkmn });
    dispatch({
      type: Actions.BATTLE_LOG_ADD, payload: {
        turn,
        text: `${party[newPkmn].name} was sent out.`
      }
    });
  }
}

export function setPhase(newPhase) {
  return dispatch => {
    dispatch({ type: BATTLE_SET_PHASE, payload: newPhase });
  }
}

export function setTurn(newTurn) {
  return dispatch => {
    dispatch({ type: BATTLE_SET_TURN, payload: newTurn });
    dispatch({ type: Actions.NPC_HAS_ACTED_TOGGLE, payload: false });
  }
}