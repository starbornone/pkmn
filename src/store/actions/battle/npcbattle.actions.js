import _ from '@lodash';

import * as Actions from 'store/actions';
import { typeStrengths, typeWeaknesses } from 'database';

export const NPC_HAS_ACTED_TOGGLE = '[NPC] HAS ACTED TOGGLE';
export const NPC_SET_HAS_ACTED = '[NPC] SET HAS ACTED';
export const NPC_SET_ACTIVE_POKEMON = '[NPC] SET ACTIVE POKEMON';

function checkMoveMatchup(userMoves, targetTypes) {
  const strongMoves = [];
  const neutralMoves = [];    // TODO:

  userMoves.map(move => {
    let effectiveness = 0;
    const outerFound = [];

    targetTypes.map((type, index) => {
      let found = _.find(typeWeaknesses[targetTypes[index].name.toLowerCase()], { name: move.type.name });

      if (found) {
        effectiveness++;
        outerFound.push(move);
      }
    });

    if (effectiveness > 0) {
      strongMoves.push({
        move: _.uniq(outerFound)[0],
        effectiveness
      });
    }
  });

  // TODO: Check for effectiveness of STAB moves
  // TODO: Account for how you could have a move that's 60 power and super effective, or a move that's 100 power and stab but just regular, and the latter would do more damage

  return strongMoves;
}

function checkTypeMatchup(userTypes, targetTypes) {
  let matchUpStrength = 0;

  let userStrength = [];
  let userWeakness = [];
  userTypes.map(type => {
    if (!_.isEmpty(typeStrengths[type.name.toLowerCase()])) {
      userStrength.push(typeStrengths[type.name.toLowerCase()][0]);
    }
    if (!_.isEmpty(typeWeaknesses[type.name.toLowerCase()])) {
      userWeakness.push(typeWeaknesses[type.name.toLowerCase()][0]);
    }
  });

  let strongTypeMatchup = [];
  userStrength.map(strength => {
    let found = _.find(targetTypes, { name: strength.name });
    if (found) {
      strongTypeMatchup.push(found);
    }
  });
  matchUpStrength += strongTypeMatchup.length;

  let weakTypeMatchup = [];
  userWeakness.map(weakness => {
    let found = _.find(targetTypes, { name: weakness.name });
    if (found) {
      weakTypeMatchup.push(found);
    }
  });
  matchUpStrength -= weakTypeMatchup.length;

  return matchUpStrength;
}

export function manageNPCBattleSwitch(turn, playerPkmn, npcPkmn) {
  return (dispatch, getState) => {
    console.log('NPC DECIDE TO SWITCH POKEMON >>>',);

    let state = getState();
    const { opponent } = state.npc;

    let partyStrengths = [];

    opponent.party.map(pkmn => {
      console.log('pkmn >>>', pkmn);
      if (pkmn.health.currentHP > 0) {
        partyStrengths.push({
          ...pkmn,
          matchup: manageNPCTypeAndMoveMatchup(playerPkmn, pkmn).matchUpTypeStrength
        });
      }
    })

    partyStrengths = _.reverse(_.sortBy(partyStrengths, ['matchup']));

    console.log('partyStrengths >>>', partyStrengths);

    let switchDesire = partyStrengths[0].matchup - _.find(partyStrengths, npcPkmn).matchup;

    // If the opponent's current active Pokemon has fainted, increase their desire to switch by a crazy amount.
    if (npcPkmn.health.currentHP <= 0) {
      switchDesire = + 100;
    }

    console.log('switchDesire >>>', switchDesire);

    if (switchDesire > 1) {
      dispatch({
        type: Actions.BATTLE_LOG_ADD, payload: {
          turn,
          text: `${opponent.name} recalled ${npcPkmn.name}.`
        }
      });
      dispatch({ type: NPC_HAS_ACTED_TOGGLE, payload: true });
      dispatch(Actions.setActivePokemon(turn, 'OPPONENT', opponent.party, partyStrengths[0].id));
    }

    console.log('NPC DECIDE TO SWITCH POKEMON END >>>',);
  }
}

function manageNPCTypeAndMoveMatchup(playerPkmn, npcPkmn) {
  let matchUpTypeStrength = checkTypeMatchup(npcPkmn.types, playerPkmn.types);

  const matchUpMoves = checkMoveMatchup(npcPkmn.moves, playerPkmn.types);
  console.log('NPC thinking: matchUpMoves >>>', matchUpMoves);

  matchUpTypeStrength += matchUpMoves.length;
  console.log('NPC thinking: matchUpTypeStrength >>>', matchUpTypeStrength);

  return {
    matchUpMoves,
    matchUpTypeStrength
  };
}

export function manageNPCBattleTurn(turn, playerPkmn, npcPkmn) {
  return dispatch => {
    console.log('NPC ATTACK TURN START >>>',);

    const { matchUpMoves } = manageNPCTypeAndMoveMatchup(playerPkmn, npcPkmn);

    let selectedMove = {};
    if (matchUpMoves.length > 0) {
      selectedMove = matchUpMoves[0].move;
    } else {
      // TODO: Determine generic move. Use RNG for now.
      selectedMove = _.find(npcPkmn.moves, { name: 'Tackle' });
    }

    dispatch(Actions.manageMove(turn, selectedMove, npcPkmn));

    console.log('NPC ATTACK TURN END >>>',);
  }
}

export function setNPCHasActed(hasActed) {
  return dispatch => {
    dispatch({ type: NPC_SET_HAS_ACTED, payload: hasActed });
  }
}