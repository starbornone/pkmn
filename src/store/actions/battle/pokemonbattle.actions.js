import _ from '@lodash';

import * as Actions from 'store/actions';

import { categories, typeStrengths, typeWeaknesses } from 'database';

export function dealDamage(turn, move, user, target) {
  return (dispatch, getState) => {
    const { targetPkmn } = target;

    const attackOrSpAtkValue = move.category.name === categories.special.name ? user.stats.spatk : move.category.name === categories.physical.name ? user.stats.attack : 0;
    const defOrSpDefValue = move.category.name === categories.special.name ? user.stats.spdef : move.category.name === categories.physical.name ? user.stats.defense : 0;

    let damageAmount = (((((2 * user.level) / 5) * move.power * (attackOrSpAtkValue / defOrSpDefValue)) / 50) + 2);

    // TODO: Modifier. The formula for this is:
    // targets * weather * critical * random * STAB * type * burn * other
    // Targets is 0.75 if the move has more than one target, and 1 otherwise.
    // Weather is 1.5 if a Water-type move is being used during rain or a Fire-type move during harsh sunlight, and 0.5 if a Water-type move is used during harsh sunlight or a Fire-type move during rain, and 1 otherwise.
    // Critical is applied starting in Generation II. It is 2 for a critical hit in Generations II-V, 1.5 for a critical hit from Generation VI onward, and 1 otherwise.
    // random is a random integer percentage between 0.85 and 1.00 (inclusive)
    damageAmount = damageAmount * (Math.floor(Math.random() * 16) + 85) / 100;
    // STAB is the same-type attack bonus. This is equal to 1.5 if the move's type matches any of the user's types, 2 if the user of the move additionally has Adaptability, and 1 if otherwise.
    console.log(user.name + ' using ' + move.name + ': damageAmount before STAB >>>', damageAmount);
    const stab = _.find(user.types, { name: move.type.name });
    console.log(user.name + ' using ' + move.name + ': is STAB? >>>', stab);
    damageAmount = !_.isEmpty(stab) ? damageAmount * 1.5 : damageAmount * 1;
    console.log(user.name + ' using ' + move.name + ': damageAmount after STAB >>>', damageAmount);
    // Type is the type effectiveness. This can be 0 (ineffective); 0.25, 0.5 (not very effective); 1 (normally effective); 2, or 4 (super effective), depending on both the move's and target's types.
    let typeStrong = 1;
    let typeWeak = 1;
    targetPkmn.types.map(targetType => {
      const foundStrong = _.find(typeWeaknesses[targetType.name.toLowerCase()], { name: move.type.name });
      if (foundStrong) {
        typeStrong = typeStrong * 2;
      }
      const foundWeak = _.find(typeStrengths[targetType.name.toLowerCase()], { name: move.type.name });
      if (foundWeak) {
        typeWeak = typeWeak * 0.5;
      }
    });
    console.log(user.name + ' using ' + move.name + ': damageAmount before typeModifier >>>', damageAmount);
    const typeModifier = typeStrong * typeWeak;
    console.log(user.name + ' using ' + move.name + ': typeModifier >>>', typeModifier);
    damageAmount = damageAmount * typeModifier;
    console.log(user.name + ' using ' + move.name + ': damageAmount after typeModifier >>>', damageAmount);
    // Burn is 0.5 if the attacker is burned, its Ability is not Guts, and the used move is a physical move, and 1 otherwise.
    // other is 1 in most cases, and a different multiplier when specific interactions of moves, Abilities, or items take effect

    console.log(user.name + ' using ' + move.name + ': total damageAmount >>>', damageAmount);

    damageAmount = Math.trunc(damageAmount);

    console.log(user.name + ' using ' + move.name + ': damageAmount truncated >>>', damageAmount);

    let state = getState();
    let dispatchType = '';
    let targetIndex = 0;
    if (target.team === 'OPPONENT') {
      dispatchType = Actions.NPC_MODIFY_POKEMON;
      targetIndex = _.findIndex(state.npc.opponent.party, { id: targetPkmn.id });
    } else {
      dispatchType = Actions.PARTY_MODIFY_POKEMON;
      targetIndex = _.findIndex(state.party.party, { id: targetPkmn.id });
    }

    const newHPAmount = targetPkmn.health.currentHP - damageAmount;
    const modifiedTarget = {
      ...targetPkmn,
      health: {
        ...targetPkmn.health,
        currentHP: newHPAmount > 0 ? newHPAmount : 0
      }
    };

    dispatch({ type: dispatchType, payload: { index: targetIndex, item: modifiedTarget } });

    let effectivenessText = '';
    if (typeModifier < 1) {
      effectivenessText = ' It wasn\'t very effective...';
    } else if (typeModifier > 1) {
      effectivenessText = ' It\'s super effective!';
    }

    dispatch({
      type: Actions.BATTLE_LOG_ADD, payload: {
        turn,
        text: `${user.name} dealt ${damageAmount} damage to ${targetPkmn.name} with ${move.name}!` + effectivenessText
      }
    })

  }
}