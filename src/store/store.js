import _ from '@lodash';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import reducer from 'store/reducers';

function saveToLocalStorage(state) {
  const serializedState = JSON.stringify(state);
  localStorage.setItem('state', serializedState);
}

function loadFromLocalStorage() {
  const serializedState = localStorage.getItem('state');
  if (serializedState === null) return undefined;
  return JSON.parse(serializedState);
}

let composeEnhancers = compose;
if (process.env.NODE_ENV !== 'production') {
  if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  }
}

const persistedState = loadFromLocalStorage();

const store = createStore(
  reducer,
  persistedState,
  composeEnhancers(applyMiddleware(thunk)),
);

store.subscribe(_.throttle(() => {
  saveToLocalStorage(store.getState());
}, 1000));

export default store;