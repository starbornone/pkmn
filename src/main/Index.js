import React from 'react';
import { Route } from 'react-router-dom';

import { routes } from 'routes';

import MusicPlayer from 'components/MusicPlayer';

import Background from 'assets/images/background.png';

function Index(props) {
  return <div className="bg-cover bg-center" style={{ backgroundImage: `url(${Background})` }}>
    {routes.map(route => {
      return <Route
        key={route.id}
        path={route.path}
        exact={route.exact}
        component={route.component}
      />
    })}
    <MusicPlayer />
  </div>
}

export default Index;