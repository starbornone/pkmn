import React from 'react';
import { useSelector } from 'react-redux';

function Map(props) {
  const location = useSelector(({ location }) => location);

  return <div>
    <img src={location.currentLocation.map.url} />
  </div>
}

export default Map;