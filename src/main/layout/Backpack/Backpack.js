import _ from '@lodash';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Item from 'components/Item';
import TabPanel from 'components/TabPanel';

import BattleItems from 'assets/images/items/storage/battle-pocket.png';
import Berries from 'assets/images/items/storage/candy-jar.png';
import KeyItems from 'assets/images/items/storage/clothing-trunk.png';
import Machines from 'assets/images/items/storage/tm-case.png';
import Medicine from 'assets/images/items/storage/medicine-pocket.png';
import MiscItems from 'assets/images/items/storage/power-up-pocket.png';
import Pokeballs from 'assets/images/items/storage/catching-pocket.png';

import { states } from 'database';

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`,
  };
}

const InventoryTabs = withStyles({
  indicator: {
    backgroundColor: '#1890ff',
  },
})(Tabs);

const InventoryTab = withStyles((theme) => ({
  root: {
    minWidth: 12,
    padding: '0'
  },
}))((props) => <Tab disableRipple {...props} />);

function Backpack(props) {
  const currentState = useSelector(({ story }) => story.currentState);
  const inventory = useSelector(({ inventory }) => inventory);
  const { medicines, miscItems } = inventory;

  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return <div>
    <AppBar
      color="default"
      elevation={0}
      position="static"
    >
      <InventoryTabs
        indicatorColor="primary"
        onChange={handleChange}
        textColor="primary"
        value={value}
        variant="fullWidth"
      >
        <InventoryTab icon={<img src={BattleItems} />} {...a11yProps(0)} />
        <InventoryTab icon={<img src={Berries} />} {...a11yProps(1)} />
        <InventoryTab icon={<img src={KeyItems} />} {...a11yProps(2)} />
        <InventoryTab icon={<img src={Machines} />} {...a11yProps(3)} />
        <InventoryTab icon={<img src={Medicine} />} {...a11yProps(4)} />
        <InventoryTab icon={<img src={MiscItems} />} {...a11yProps(5)} />
        <InventoryTab icon={<img src={Pokeballs} />} {...a11yProps(6)} />
      </InventoryTabs>
    </AppBar>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={0}>
    </TabPanel>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={1}>
    </TabPanel>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={2}>
    </TabPanel>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={3}>
    </TabPanel>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={4}>
      {medicines.map((item, index) => <Item currentState={currentState} item={item} key={index} />)}
    </TabPanel>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={5}>
      {miscItems.map((item, index) => <Item currentState={currentState} item={item} key={index} />)}
    </TabPanel>
    <TabPanel className="overflow-x-auto" style={{ height: '400px' }} value={value} index={6}>
    </TabPanel>
  </div>
}

export default Backpack;