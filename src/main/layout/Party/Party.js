import React from 'react';
import { useSelector } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import HealthBar from 'components/HealthBar';

function Party(props) {
  const party = useSelector(({ party }) => party.party);

  return <div>
    <Grid
      alignItems="stretch"
      className="h-full py-4"
      container
      direction="column"
      justify="flex-start"
      spacing={1}
    >
      {party.map(pokemon => <Grid className="flex flex-col items-center" item key={pokemon.id}>
        <img
          className="bg-gray-100 rounded-full"
          src={pokemon.sprite.main.url}
          style={{ height: pokemon.sprite.main.height, width: pokemon.sprite.main.width }}
        />
        <Typography variant="subtitle1">{pokemon.name}</Typography>
        <div className="w-full px-4">
          <HealthBar health={pokemon.health} />
        </div>
      </Grid>)}
    </Grid>
  </div>
}

export default Party;