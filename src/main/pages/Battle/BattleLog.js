import _ from '@lodash';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Message from 'components/Message';

import { setPhase } from 'store/actions';

function BattleLog(props) {
  const dispatch = useDispatch();

  const battle = useSelector(({ battle }) => battle);

  const testLog = [];
  battle.log.map(log => {
    testLog[log.turn] = {
      turn: log.turn,
      text: []
    }
  })
  battle.log.map(log => {
    testLog[log.turn].text.push(log.text);
  })
  console.log('testLog >>>', testLog);
  console.log('battle.turn >>>', battle.turn);
  console.log('testLog[battle.turn] >>>', testLog[battle.turn - 1]);

  const messages = testLog[battle.turn - 1].text;

  const [currentMessage, setCurrentMessage] = useState(0);
  const handleClick = () => {
    if (currentMessage < messages.length - 1) {
      setCurrentMessage(currentMessage + 1);
    } else {
      dispatch(setPhase('PLAYER_PICKING_TURN'));
    }
  };

  return <div
    className="px-1 w-full"
    onClick={handleClick}
    style={{ height: '74px' }}
  >
    <Message message={messages[currentMessage]} key={currentMessage} />
  </div>
}

export default BattleLog;