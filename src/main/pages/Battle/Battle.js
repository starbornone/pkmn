import _ from '@lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import HealthBar from 'components/HealthBar';

import { manageTurn, replenishHP, replenishPP } from 'store/actions';
import { battlePhases } from 'database';

import { hashFnv32a } from 'utilities';

import BattleLog from './BattleLog';

function Battle(props) {
  const dispatch = useDispatch();

  const battle = useSelector(({ battle }) => battle);
  const { battlePhase, turn } = battle;

  const party = useSelector(({ party }) => party);
  const currentPkmn = party.party[party.activePokemon];

  const opponent = useSelector(({ npc }) => npc.opponent);
  const rivalPkmn = opponent.party[opponent.activePokemon];

  function handleManageMove(move) {
    dispatch(manageTurn(turn, move, currentPkmn, rivalPkmn));
  }

  function handleResetBattle() {
    dispatch(replenishHP('PARTY', currentPkmn));
    dispatch(replenishPP('PARTY', currentPkmn));

    dispatch(replenishHP('OPPONENT', rivalPkmn));
    dispatch(replenishPP('OPPONENT', rivalPkmn));
  }

  let movesOrResult;

  switch (battlePhase) {
    case battlePhases.BATTLE_WON_PLAYER: {
      movesOrResult = <div>Win!</div>;
      break;
    }
    case battlePhases.BATTLE_WON_OPPONENT: {
      movesOrResult = <div>Lose!</div>;
      break;
    }
    case battlePhases.TURN_RESULTS: {
      movesOrResult = <Grid item xs={12}>
        <Box width="504px">
          <BattleLog />
        </Box>
      </Grid>;
      break;
    }
    default: {
      movesOrResult = currentPkmn.moves.map(move => <Grid item key={hashFnv32a(move.name)} xs={6}>
        <Button
          className="h-full p-2 text-left normal-case leading-normal"
          disabled={move.currentPP <= 0}
          onClick={() => handleManageMove(move)}
          type="button"
          variant="outlined"
        >
          <div>
            <div>{move.name}</div>
            <div><span style={{ color: move.type.colour }}>{move.type.name}</span>, {move.category.name}</div>
            <div className="flex">
              <div className="w-1/3 text-gray-500">Pow: {move.power > 0 ? move.power : '-'}</div>
              <div className="w-1/3 text-gray-500">Acc: {move.accuracy > 0 ? `${move.accuracy}%` : '-'}</div>
              <div className="w-1/3 text-gray-500">PP: {move.currentPP} / {move.pp}</div>
            </div>
            <Typography variant="caption">{move.description}</Typography>
          </div>
        </Button>
      </Grid>)
      break;
    }
  }

  return <Grid
    container
    direction="column"
    justify="flex-start"
    alignItems="center"
    spacing={3}
  >
    <Grid item xs={12}>
      <div className="p-4">

        <Grid
          className="-mt-4"
          container
          justify="flex-end"
          spacing={1}
        >
          <Grid item xs={5}>
            <img src={rivalPkmn.sprite.main.url} style={{ height: rivalPkmn.sprite.main.height, width: rivalPkmn.sprite.main.width }} />
            <div className="-mt-6 p-2">
              <Typography variant="subtitle1">{rivalPkmn.name}</Typography>
              <HealthBar health={rivalPkmn.health} />
            </div>
          </Grid>
        </Grid>

        <Grid
          className="-mt-20"
          container
          justify="flex-start"
          spacing={1}
        >
          <Grid item xs={5}>
            <img src={currentPkmn.sprite.back.url} style={{ height: currentPkmn.sprite.back.height, width: currentPkmn.sprite.back.width }} />
            <div className="-mt-6 p-2">
              <Typography variant="subtitle1">{currentPkmn.name}</Typography>
              <HealthBar health={currentPkmn.health} />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div>
              <Grid
                alignItems="stretch"
                className="flex content-start"
                container
                direction="row"
                justify="flex-start"
                spacing={1}
              >
                {movesOrResult}
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12}>

            <Grid
              container
              spacing={1}
            >
              <Grid item xs={6}>
                <Button
                  className="w-full border-gray-300"
                  onClick={() => { }}
                  size="small"
                  type="button"
                  variant="outlined"
                >Flee</Button>
              </Grid>
              <Grid item xs={6}>
                <Button
                  className="w-full border-gray-300"
                  color="danger"
                  onClick={handleResetBattle}
                  size="small"
                  type="button"
                  variant="outlined"
                >[DEBUG: Reset]</Button>
              </Grid>
            </Grid>

          </Grid>
        </Grid>

      </div>
    </Grid>
  </Grid>
}

export default Battle;