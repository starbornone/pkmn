import React from 'react';
import { useDispatch } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Button from 'components/Button';

import Intro01 from 'assets/images/animated/intro-01.gif';
import Intro02 from 'assets/images/animated/intro-02.gif';
import Intro03 from 'assets/images/animated/intro-03.gif';
import Pokeball from 'assets/images/items/ball/poke.png'

import { changeStoryState, resetState } from 'store/actions';
import { states } from 'database';

function Home(props) {
  const dispatch = useDispatch();

  function handleChangeStoryState() {
    dispatch(changeStoryState(states.STORYSTATE, 'charactercreate'));
  }

  function handleResetState() {
    dispatch(resetState());
  }

  return <Grid
    alignItems="center"
    container
    className="h-screen w-screen bg-gray-300"
    direction="column"
    justify="center"
  >
    <Grid item>
      <Typography className="p-0 m-0" variant="h1">PKMN</Typography>
    </Grid>
    <Grid item>
      <Grid
        alignItems="center"
        container
        direction="row"
        justify="center"
        spacing={3}
      >
        <Grid item>
          <img src={Intro01} style={{ width: '300px' }} />
        </Grid>
        <Grid item>
          <img src={Intro02} style={{ width: '300px' }} />
        </Grid>
        <Grid item>
          <img src={Intro03} style={{ width: '300px' }} />
        </Grid>
      </Grid>
    </Grid>
    <Grid item>
      <Button
        className="mt-6 mb-4"
        color="primary"
        onClick={() => handleChangeStoryState()}
        href="/new-game"
        variant="contained"
      ><img src={Pokeball} style={{ height: '32px', width: '32px' }} />Start Game</Button>
    </Grid>
    <Grid item>
      <Button
        className="normal-case"
        color="danger"
        onClick={() => handleResetState()}
        size="small"
        type="button"
        variant="outlined"
      >[DEBUG: Reset State]</Button>
    </Grid>
  </Grid>
}

export default Home;