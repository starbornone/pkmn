import React from 'react';
import { useSelector } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Backpack from 'main/layout/Backpack/Backpack';
import Map from 'main/layout/Map/Map';
import Party from 'main/layout/Party/Party';

import CharacterCreate from 'main/pages/NewGame/CharacterCreate';
import GameContent from 'main/pages/Game/GameContent';
import StarterSelect from 'main/pages/NewGame/StarterSelect';

function Game(props) {
  const story = useSelector(({ story }) => story);
  const { currentState } = story;

  return <Grid
    alignItems="center"
    container
    direction="row"
    justify="center"
    spacing={0}
    style={{ minHeight: '100vh' }}
  >
    <Grid item style={{ width: '960px' }}>
      <Paper className="bg-gray-100" elevation={6} square>
        <Grid
          alignItems="stretch"
          className="flex items-stretch overflow-hidden"
          container
          direction="row"
          spacing={0}
          style={{ height: '646px' }}
        >
          <Grid className="bg-gray-200" item xs={2}>
            <Party />
          </Grid>
          <Grid className="h-full p-4" item xs={7}>
            <div className="h-full w-full overflow-x-hidden overflow-y-auto">
              <GameContent />
            </div>
          </Grid>
          <Grid className="bg-gray-200" item xs={3}>
            <Map />
            <Backpack />
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  </Grid>
}

export default Game;