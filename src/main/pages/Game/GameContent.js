import React from 'react';
import { useSelector } from 'react-redux';

import { states } from 'database';

import Battle from 'main/pages/Battle/Battle';
import Location from 'main/pages/Location/Location';
import Pokemon from 'main/pages/Pokemon/Pokemon';
import Story from 'main/pages/Story/Story';

function GameContent(props) {
  const currentState = useSelector(({ story }) => story.currentState);

  let componentToDisplay = <div></div>;

  switch (currentState.type) {

    case states.BATTLESTATE: {
      componentToDisplay = <Battle />;
      break;
    }

    case states.LOCATIONSTATE: {
      componentToDisplay = <Location />;
      break;
    }

    case states.POKEMONSTATE: {
      componentToDisplay = <Pokemon />;
      break;
    }

    case states.STORYSTATE: {
      componentToDisplay = <Story />;
      break;
    }

    default: {
      break;
    }
  }

  return componentToDisplay;
}

export default GameContent;