import _ from '@lodash';
import Formsy from 'formsy-react';
import React, { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';

import ButtonGroup from '@material-ui/core/ButtonGroup';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Button from 'components/Button';

import { addStarterToParty, changeStoryState, setupRival } from 'store/actions';
import { basePokemon, states, stats, starters } from 'database';
import { hashFnv32a } from 'utilities';

function StarterSelect(props) {
  const dispatch = useDispatch();

  const [isFormValid, setIsFormValid] = useState(false);
  const formRef = useRef(null);

  const [starterIndex, setStarterIndex] = useState(0);

  function disableButton() {
    setIsFormValid(false);
  }
  function enableButton() {
    setIsFormValid(true);
  }

  function handleChangeStoryState(newStateType, newStateName) {
    dispatch(changeStoryState(newStateType, newStateName));
  }

  function handleSetStarter(move) {
    let newIndex = starterIndex;
    switch (move) {
      case 'back': {
        newIndex = starterIndex - 1;
        if (newIndex < 0) {
          newIndex = starters.length - 1;
          break;
        }
        break;
      }
      case 'next': {
        newIndex = starterIndex + 1;
        if (newIndex >= starters.length) {
          newIndex = 0;
          break;
        }
        break;
      }
    }
    setStarterIndex(newIndex);
  }

  function handleSubmit() {
    dispatch(setupRival(starters[starterIndex]));
    dispatch(addStarterToParty(starters[starterIndex]));
  }

  return <Formsy
    onValidSubmit={handleSubmit}
    onValid={enableButton}
    onInvalid={disableButton}
    ref={formRef}
    className="flex flex-col justify-center w-full p-16"
  >
    <Grid
      className="mt-2"
      container
    >
      <Grid item xs={12}>

        <Paper className="p-6" variant="outlined">

          <Grid
            className="mb-2"
            container
            spacing={0}
          >
            <Grid item xs={3}>

              <Grid
                className="-mb-1"
                container
                spacing={0}
              >
                <Grid className="-ml-6" item>
                  <img src={starters[starterIndex].sprite.main.url} style={{ height: starters[starterIndex].sprite.main.height, width: starters[starterIndex].sprite.main.width }} />
                </Grid>
                <Grid className="-ml-12" item>
                  <img src={starters[starterIndex].sprite.back.url} style={{ height: starters[starterIndex].sprite.back.height, width: starters[starterIndex].sprite.back.width }} />
                </Grid>
              </Grid>
              <Typography className="-mb-2" variant="h6">{starters[starterIndex].name}</Typography>
              <Typography className="text-gray-500" variant="subtitle1">level {starters[starterIndex].level}</Typography>

            </Grid>
            {starters[starterIndex].evolutions.map(evolution => {
              const pokemonDetails = _.find(basePokemon, { id: evolution.id });
              return <Grid className="opacity-25" item key={pokemonDetails.id} xs={3}>
                <img
                  src={pokemonDetails.sprite.main.url}
                  style={{ height: pokemonDetails.sprite.main.height, width: pokemonDetails.sprite.main.width }}
                />
                <Typography variant="caption">Evolves at level {evolution.level}<br />into {pokemonDetails.name}</Typography>
              </Grid>
            })}
          </Grid>

          <Grid
            className="mb-2"
            container
            spacing={1}
          >
            {starters[starterIndex].types.map(type => <Grid item key={type.name}>
              <Chip
                label={type.name}
                size="small"
                style={{ backgroundColor: type.colour }}
              />
            </Grid>)}
          </Grid>

          <Grid
            className="mb-1"
            container
            spacing={3}
          >
            <Grid item xs={2}>

              <Typography variant="subtitle1">Stats</Typography>
              {stats.map(stat => <div className="flex justify-between" key={hashFnv32a(stat.keyValue)}>
                <div className="w-2/3 text-left text-gray-500">
                  {stat.name}</div>
                <div className="w-1/3 text-right">
                  {starters[starterIndex].stats[stat.keyValue]}
                </div>
              </div>)}

            </Grid>
            <Grid item xs={2}>

              <Typography className="-mb-4" variant="subtitle1">Abilities</Typography>
              {starters[starterIndex].abilities.map(ability => <div className="mt-4" key={hashFnv32a(ability.name)}>
                <div>{ability.name}</div>
                <Typography className="text-gray-500" variant="caption">{ability.description}</Typography>
              </div>)}

            </Grid>
            <Grid item xs={8}>

              <Typography variant="subtitle1">Moves</Typography>
              <Grid
                container
                spacing={3}
              >
                {starters[starterIndex].moves.map(move => <Grid item key={hashFnv32a(move.name)} xs={6}>

                  <Grid
                    container
                    spacing={0}
                  >
                    <Grid item xs={6}>
                      {move.name}
                    </Grid>
                    <Grid className="text-right" item xs={6}>
                      <span style={{ color: move.type.colour }}>{move.type.name}</span>, {move.category.name}
                    </Grid>
                  </Grid>
                  <div className="flex justify-between">
                    <div className="w-1/3 text-left text-gray-500">Pow: {move.power > 0 ? move.power : '-'}</div>
                    <div className="w-1/3 text-center text-gray-500">Acc: {move.accuracy > 0 ? `${move.accuracy}%` : '-'}</div>
                    <div className="w-1/3 text-right text-gray-500">PP: {move.pp}</div>
                  </div>
                  <Typography variant="caption">{move.description}</Typography>

                </Grid>)}
              </Grid>

            </Grid>
          </Grid>

        </Paper>

        <Grid
        className="mt-2"
          container
          spacing={3}
        >
          <Grid className="pb-0" item xs={3}>
            <Button
              className="normal-case w-full"
              onClick={() => handleChangeStoryState(states.STORYSTATE, 'charactercreate')}
              type="button"
              variant="outlined"
            >Back</Button>
          </Grid>
          <Grid className="pb-0" item xs={9}>
            <ButtonGroup className="w-full">
              <Button
                className="normal-case w-1/5"
                color="secondary"
                disableElevation
                onClick={() => handleSetStarter('back')}
                type="button"
                variant="contained"
              >Last Pokemon</Button>
              <Button
                className="normal-case w-3/5"
                color="primary"
                disabled={!isFormValid}
                disableElevation
                type="submit"
                variant="contained"
              >Select {starters[starterIndex].name}</Button>
              <Button
                className="normal-case w-1/5"
                color="secondary"
                disableElevation
                onClick={() => handleSetStarter('next')}
                type="button"
                variant="contained"
              >Next Pokemon</Button>
            </ButtonGroup>
          </Grid>
        </Grid>

      </Grid>
    </Grid>

  </Formsy>
}

export default StarterSelect;