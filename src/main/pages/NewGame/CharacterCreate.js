import _ from '@lodash';
import Formsy from 'formsy-react';
import React, { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import Typography from '@material-ui/core/Typography';

import Button from 'components/Button';
import Input from 'components/Input';
import RadioInput from 'components/RadioInput';
import Select from 'components/Select';

import { changeStoryState, characterCreation } from 'store/actions';
import { characterModels, states } from 'database';
import { hashFnv32a } from 'utilities';

function CharacterCreate(props) {
  const dispatch = useDispatch();

  const [isFormValid, setIsFormValid] = useState(false);
  const formRef = useRef(null);

  function disableButton() {
    setIsFormValid(false);
  }
  function enableButton() {
    setIsFormValid(true);
  }

  function handleChangeStoryState(newStateType, newStateName) {
    dispatch(changeStoryState(newStateType, newStateName));
  }

  function handleSubmit(model) {
    dispatch(characterCreation(model));
  }

  return <Formsy
    onValidSubmit={handleSubmit}
    onValid={enableButton}
    onInvalid={disableButton}
    ref={formRef}
    className="flex items-center content-center h-full w-full p-16"
  >
    <Grid
      container
      spacing={3}
    >
      <Grid item xs={12}>
        <Typography variant="h4">Create your character</Typography>
      </Grid>
      <Grid item xs={12}>

        <RadioInput
          name="character.sprite"
          required
        >
          <Grid
            container
            justify="space-between"
            spacing={1}
          >
            {characterModels.map(model => <Grid item key={hashFnv32a(model.fieldValue)} xs={4}>

              <FormControlLabel
                control={<Radio />}
                label={<div>

                  <Grid
                    alignItems="flex-end"
                    container
                    spacing={1}
                  >
                    <Grid item xs={6}>
                      <div style={{ height: model.sprite.main.height, width: model.sprite.main.width }}>
                        <img src={model.sprite.main.url} style={{ height: model.sprite.main.height, width: model.sprite.main.width }} />
                      </div>
                    </Grid>
                    <Grid item xs={6}>
                      <div style={{ height: model.sprite.close.height, width: model.sprite.close.width }}>
                        <img src={model.sprite.close.url} style={{ height: model.sprite.close.height, width: model.sprite.close.width }} />
                      </div>
                    </Grid>
                  </Grid>

                </div>}
                value={model.fieldValue}
              />
            </Grid>)}
          </Grid>
        </RadioInput>

      </Grid>
      <Grid item xs={6}>

        <Input
          className="w-full"
          name="character.name.first"
          label="First name"
          required
          value="Sha"
        />

      </Grid>
      <Grid item xs={6}>

        <Input
          className="w-full"
          name="character.name.last"
          label="Last name"
          required
          value="Kong-Brooks"
        />

      </Grid>
      <Grid item xs={6}>

        <Input
          className="w-full"
          name="character.birthdate"
          InputLabelProps={{
            shrink: true,
          }}
          label="Birthdate"
          required
          type="date"
          value="1989-05-20"
        />

      </Grid>
      <Grid item xs={6}>

        <Select
          className="w-full"
          label="Home Region"
          name="character.region"
          required
          value="Johto"
        >
          <MenuItem value="Johto">Johto</MenuItem>
        </Select>

      </Grid>
      <Grid item xs={6}>
        <Button
          className="normal-case w-full"
          href="/"
          onClick={() => handleChangeStoryState(states.STORYSTATE, 'titlescreen')}
          type="button"
          variant="outlined"
        >Back</Button>
      </Grid>
      <Grid item xs={6}>
        <Button
          className="normal-case w-full"
          color="primary"
          disabled={!isFormValid}
          disableElevation
          type="submit"
          variant="contained"
        >Next</Button>
      </Grid>
    </Grid>
  </Formsy>
}

export default CharacterCreate;