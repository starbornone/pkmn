import React from 'react';
import { useSelector } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import CharacterCreate from 'main/pages/NewGame/CharacterCreate';
import StarterSelect from 'main/pages/NewGame/StarterSelect';

import { states } from 'database';

function NewGame(props) {
  const story = useSelector(({ story }) => story);
  const { currentState } = story;

  let componentToDisplay = <div></div>;
  switch (currentState.type) {
    case states.STORYSTATE: {

      switch (currentState.name) {
        case 'charactercreate': {
          componentToDisplay = <CharacterCreate />;
          break;
        }
        case 'starterintro': {
          componentToDisplay = <div></div>;
          break;
        }
        case 'starterselect': {
          componentToDisplay = <StarterSelect />;
          break;
        }
        default: {
          break;
        }
      }

    }
    default: {
      break;
    }
  }

  return <Grid
    alignItems="center"
    container
    direction="row"
    justify="center"
    spacing={0}
    style={{ minHeight: '100vh' }}
  >
    <Grid item style={{ width: '960px' }}>
      <Paper className="bg-gray-100" elevation={6} square>
        <div className="overflow-x-hidden overflow-y-auto" style={{ height: '646px' }}>
          {componentToDisplay}
        </div>
      </Paper>
    </Grid>
  </Grid>
}

export default NewGame;