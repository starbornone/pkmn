export const themeColours = {
  palette: {
    primary: {
      dark: '#2e5c95',
      light: '#94b9f9',
      main: '#6289c6',
    },
    secondary: {
      dark: '#8faa5f',
      light: '#f3ffbe',
      main: '#c0dc8d',
    },
    danger: {
      dark: '#b2192e',
      light: '#ff8684',
      main: '#ea5357',
    },
    warning: {
      dark: '#be9f3b',
      light: '#ffff9a',
      main: '#f3d06a',
    },
  },
};