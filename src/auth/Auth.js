import React, { Component } from 'react';
import * as userActions from 'auth/store/actions';
import * as Actions from 'store/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Auth extends Component {
  state = {
    waitAuthCheck: true
  };

  componentDidMount() {
    return Promise.all([
      // this.jwtCheck()
    ]).then(() => {
      this.setState({ waitAuthCheck: false });
    });
  }

  jwtCheck = () =>
    new Promise(resolve => {
      jwtService.on('onAutoLogin', () => {
        this.props.showMessage({ message: 'Logging in with JWT' });
        jwtService.signInWithToken()
          .then(user => {
            this.props.setUserData(user);
            resolve();
            this.props.showMessage({ message: 'Logged in with JWT' });
          })
          .catch(error => {
            this.props.showMessage({ message: error });
            resolve();
          });
      });
      jwtService.on('onAutoLogout', message => {
        if (message) {
          this.props.showMessage({ message });
        }
        this.props.logout();
        resolve();
      });
      jwtService.on('onNoAccessToken', () => {
        resolve();
      });
      jwtService.init();
      return Promise.resolve();
    });

  render() {
    return this.state.waitAuthCheck ? <div>splash screen</div> : <>{this.props.children}</>;
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      logout: userActions.logoutUser,
      setUserData: userActions.setUserData,
      setUserDataAuth0: userActions.setUserDataAuth0,
      setUserDataFirebase: userActions.setUserDataFirebase,
      showMessage: Actions.showMessage,
      hideMessage: Actions.hideMessage
    },
    dispatch
  );
}

export default connect(null, mapDispatchToProps)(Auth);
