import history from '@history';
import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';

import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { darkTheme } from 'assets/dark.theme';
import Index from 'main/Index';

const App = () => {
  return <Router history={history}>
    <Switch>
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
        <Route path="/" component={Index} />
      </ThemeProvider>
    </Switch>
  </Router>
}

export default App;