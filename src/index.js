import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import * as serviceWorker from './serviceWorker';
import store from './store/store';

import App from './App';

/** FontAwesome */
import './assets/fontawesome/fontawesome';
import './assets/fontawesome/brands';
import './assets/fontawesome/light';

/** Custom */
import './assets/styles/tailwind.css';

const target = document.getElementById('root');

render(<Provider store={store}>
  <App />
</Provider>, target);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();