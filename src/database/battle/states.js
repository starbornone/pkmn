import BattleTrainer from 'assets/audio/battle-trainer.mp3';

import { states } from 'database';

export const battleStates = {
  battletrainer: {
    name: "battletrainer",
    music: [
      {
        musicSrc: BattleTrainer,
        name: "Battle (Trainer Battle)"
      }
    ],
    type: states.BATTLESTATE
  }
}