import _ from '@lodash';

import { generatePokemon } from 'store/actions/pokemon/pokemon.actions';
import { basePokemon } from 'database';

const iv = 31;
const ev = 252;
const level = 5;

const bulbasaur = generatePokemon(_.find(basePokemon, { id: 1 }), level, iv, ev);
const charmander = generatePokemon(_.find(basePokemon, { id: 4 }), level, iv, ev);
const squirtle = generatePokemon(_.find(basePokemon, { id: 7 }), level, iv, ev);

export const starters = [
  bulbasaur,
  charmander,
  squirtle
];