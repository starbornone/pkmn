import _ from '@lodash';

import { abilities, evolutionRequirements, miscItems, moves, time, types } from 'database';
import { sprites } from 'database/pokemon/sprites';

export const basePokemon = [
  {
    id: 1,
    name: "Bulbasaur",
    sprite: {
      main: {
        url: sprites.bulbasaur.front,
        height: '96px',
        width: '96px'
      },
      back: {
        url: sprites.bulbasaur.back,
        height: '96px',
        width: '96px'
      },
    },
    details: {
      height: "70",
      weight: "690",
      description: "There is a plant seed on its back right from the day this Pokémon is born. The seed slowly grows larger."
    },
    types: [
      types.grass,
      types.poison
    ],
    baseStats: {
      hp: 45,
      attack: 49,
      defense: 49,
      spatk: 65,
      spdef: 65,
      speed: 45
    },
    abilities: [
      abilities.overgrow
    ],
    moves: [
      {
        ...moves.tackle,
        level: 1
      },
      {
        ...moves.growl,
        level: 1
      },
      {
        ...moves.vineWhip,
        level: 3
      },
      {
        ...moves.growth,
        level: 6
      },
      {
        ...moves.leechSeed,
        level: 9
      }
    ],
    evolutions: [
      {
        id: 2,
        level: 16
      },
      {
        id: 3,
        level: 32
      }
    ]
  }, {
    id: 2,
    name: "Ivysaur",
    sprite: {
      main: {
        url: sprites.ivysaur.front,
        height: '96px',
        width: '96px'
      }
    },
    details: {
    },
    types: [
      types.grass,
      types.poison
    ],
    baseStats: {
    },
    abilities: [
    ],
    moves: [
    ],
    evolutions: [
      {
        id: 3,
        level: 32
      }
    ]
  }, {
    id: 3,
    name: "Venusaur",
    sprite: {
      main: {
        url: sprites.venusaur.front,
        height: '96px',
        width: '96px'
      }
    },
    details: {
    },
    types: [
      types.grass,
      types.poison
    ],
    baseStats: {
    },
    abilities: [
    ],
    moves: [
    ]
  }, {
    id: 4,
    name: "Charmander",
    sprite: {
      main: {
        url: sprites.charmander.front,
        height: '96px',
        width: '96px'
      },
      back: {
        url: sprites.charmander.back,
        height: '96px',
        width: '96px'
      },
    },
    details: {
      height: "60",
      weight: "850",
      description: "From the time it is born, a flame burns at the tip of its tail. Its life would end if the flame were to go out."
    },
    types: [
      types.fire
    ],
    baseStats: {
      hp: 39,
      attack: 52,
      defense: 43,
      spatk: 60,
      spdef: 50,
      speed: 65
    },
    abilities: [
      abilities.blaze
    ],
    moves: [
      {
        ...moves.scratch,
        level: 1
      },
      {
        ...moves.growl,
        level: 1
      },
      {
        ...moves.ember,
        level: 4
      },
      {
        ...moves.smokescreen,
        level: 8
      }
    ],
    evolutions: [
      {
        id: 5,
        level: 16
      },
      {
        id: 6,
        level: 32
      }
    ]
  }, {
    id: 5,
    name: "Charmeleon",
    sprite: {
      main: {
        url: sprites.charmeleon.front,
        height: '96px',
        width: '96px'
      }
    },
    details: {
    },
    types: [
      types.fire
    ],
    baseStats: {
    },
    abilities: [
    ],
    moves: [
    ],
    evolutions: [
      {
        id: 6,
        level: 32
      }
    ]
  }, {
    id: 6,
    name: "Charizard",
    sprite: {
      main: {
        url: sprites.charizard.front,
        height: '96px',
        width: '96px'
      }
    },
    details: {
    },
    types: [
      types.fire
    ],
    baseStats: {
    },
    abilities: [
    ],
    moves: [
    ],
    evolutions: []
  }, {
    id: 7,
    name: "Squirtle",
    sprite: {
      main: {
        url: sprites.squirtle.front,
        height: '96px',
        width: '96px'
      },
      back: {
        url: sprites.squirtle.back,
        height: '96px',
        width: '96px'
      },
    },
    details: {
      height: "50",
      weight: "900",
      description: "After birth, its back swells and hardens into a shell. Powerfully sprays foam from its mouth."
    },
    types: [
      types.water
    ],
    baseStats: {
      hp: 44,
      attack: 48,
      defense: 65,
      spatk: 50,
      spdef: 64,
      speed: 43
    },
    abilities: [
      abilities.torrent
    ],
    moves: [
      {
        ...moves.tackle,
        level: 1
      },
      {
        ...moves.tailWhip,
        level: 1
      },
      {
        ...moves.waterGun,
        level: 3
      },
      {
        ...moves.withdraw,
        level: 6
      },
      {
        ...moves.rapidSpin,
        level: 9
      }
    ],
    evolutions: [
      {
        id: 8,
        level: 16
      },
      {
        id: 9,
        level: 32
      }
    ]
  }, {
    id: 8,
    name: "Wartortle",
    sprite: {
      main: {
        url: sprites.wartortle.front,
        height: '96px',
        width: '96px'
      },
    },
    details: {
    },
    types: [
      types.water
    ],
    baseStats: {
    },
    abilities: [
    ],
    moves: [
    ],
    evolutions: [
      {
        id: 9,
        level: 32
      }
    ]
  }, {
    id: 9,
    name: "Blastoise",
    sprite: {
      main: {
        url: sprites.blastoise.front,
        height: '96px',
        width: '96px'
      },
    },
    details: {
    },
    types: [
      types.water
    ],
    baseStats: {
    },
    abilities: [
    ],
    moves: [
    ],
    evolutions: []
  },
  {
    id: 133,
    name: "Eevee",
    sprite: {
      main: {
        url: sprites.eevee.front,
        height: '96px',
        width: '96px'
      },
      back: {
        url: sprites.eevee.back,
        height: '96px',
        width: '96px'
      },
    },
    details: {
      height: "30",
      weight: "650",
      description: "Thanks to its unstable genetic makeup, this special Pokémon conceals many different possible evolutions."
    },
    types: [
      types.normal
    ],
    baseStats: {
      hp: 55,
      attack: 55,
      defense: 50,
      spatk: 45,
      spdef: 65,
      speed: 55
    },
    abilities: [
      // abilities.adapatbility,
      // abilities.runAway
    ],
    moves: [
      {
        ...moves.covet,
        level: 1
      },
      {
        ...moves.helpingHand,
        level: 1
      },
      {
        ...moves.tackle,
        level: 1
      },
      {
        ...moves.growl,
        level: 1
      },
      {
        ...moves.tailWhip,
        level: 1
      },
      {
        ...moves.sandAttack,
        level: 5
      }
    ],
    evolutions: [
      {
        // Vaporeon
        id: 134,
        otherRequirements: [
          {
            type: evolutionRequirements.ITEM,
            value: miscItems.waterstone
          }
        ]
      },
      {
        // Jolteon
        id: 135,
        otherRequirements: [
          {
            type: evolutionRequirements.ITEM,
            value: miscItems.thunderstone
          }]
      },
      {
        // Flareon
        id: 136,
        otherRequirements: [
          {
            type: evolutionRequirements.ITEM,
            value: miscItems.firestone
          }]
      },
      {
        // Espeon
        id: 196,
        level: 0,
        otherRequirements: [
          {
            type: evolutionRequirements.FRIENDSHIP,
            value: 220
          },
          {
            type: evolutionRequirements.TIME,
            value: time.day
          }
        ]
      },
      {
        // Umbreon
        id: 197,
        level: 0,
        otherRequirements: [
          {
            type: evolutionRequirements.FRIENDSHIP,
            value: 220
          },
          {
            type: evolutionRequirements.TIME,
            value: time.night
          }
        ]
      },
      {
        // Leafeon
        id: 470,
        otherRequirements: [
          {
            type: evolutionRequirements.ITEM,
            value: miscItems.leafstone
          }]
      },
      {
        // Glaceon
        id: 471,
        otherRequirements: [
          {
            type: evolutionRequirements.ITEM,
            value: miscItems.icestone
          }]
      },
      {
        // Sylveon
        id: 700,
        level: 0,
        otherRequirements: [
          {
            type: evolutionRequirements.FRIENDSHIP,
            value: 160
          },
          {
            type: evolutionRequirements.MOVE,
            value: types.fairy
          }
        ]
      }
    ]
  }
]