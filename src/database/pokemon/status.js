import _ from '@lodash';

import { categories, cures, effects, exceptions, stats, targets, types } from 'database';

export const status = {
  burn: {
    effects: [
      {
        type: effects.DAMAGE,
        amount: "1/16",
        target: targets.SELF
      },
      {
        type: effects.AFFECT_MOVE_CATEGORY,
        amount: "1/2",
        target: categories.PHYSICAL
      }
    ],
    exceptions: [
      {
        type: exceptions.POKEMON_TYPE,
        value: types.fire
      }
    ],
    names: {
      general: "burn",
      gain: "has been burned",
      effect: "is hurt by its burn",
      cure: "is no longer burned"
    },
    ui: {
      color: "#f08030",
      label: "BRN"
    }
  },
  faint: {
    effect: [
      {
        type: effects.CANNOT_BATTLE
      }
    ],
    names: {
      general: "faint",
      gain: "has fainted",
      effect: "is fainted",
      cure: "has been revived"
    },
    ui: {
      color: "#c03028",
      label: "FNT"
    }
  },
  frozen: {
    cures: [
      {
        type: cures.CHANCE,
        value: "0.20"
      }, {
        type: cures.ATTACK,
        value: types.fire
      }
    ],
    effect: [
      {
        type: effects.TURN_SKIP
      }
    ],
    names: {
      general: "frozen",
      gain: "was frozen solid",
      effect: "is frozen solid",
      cure: "is no longer frozen"
    },
    ui: {
      color: "#98d8d8",
      label: "FRZ"
    }
  },
  paralysis: {
    effect: [
      {
        type: effects.TURN_SKIP,
        chance: "0.25"
      }, {
        type: effects.STAT_CHANGE,
        amount: "0.75",
        target: _.find(stats, { keyValue: 'speed' })
      }
    ],
    exceptions: [
      {
        type: exceptions.POKEMON_TYPE,
        value: types.electric
      }
    ],
    names: {
      general: "paralysis",
      gain: "has become paralysed",
      effect: "is paralysed. It can't move",
      cure: "is no longer paralysed"
    },
    ui: {
      color: "#f8d030",
      label: "PAR"
    }
  },
  poison: {
    effect: [
      {
        type: effects.DAMAGE,
        amount: "1/16",
        target: targets.SELF
      }
    ],
    names: {
      general: "poison",
      gain: "has become poisoned",
      effect: "is hurt by poison",
      cure: "is no longer poisoned"
    },
    ui: {
      color: "#a040a0",
      label: "PSN"
    }
  },
  sleep: {
    cures: [
      {
        type: cures.CHANCE,
        value: "1/3"
      }
    ],
    effect: [
      {
        type: effects.TURN_SKIP
      }
    ],
    names: {
      general: "sleep",
      gain: "has fallen asleep",
      effect: "is fast asleep",
      cure: "has woken up"
    },
    ui: {
      color: "#8c888c",
      label: "SLP"
    }
  }
}