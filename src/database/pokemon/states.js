import Evolution from 'assets/audio/evolution.mp3';

import { states } from 'database';

export const pokemonStates = {
  evolution: {
    name: "evolution",
    music: [
      {
        musicSrc: Evolution,
        name: "Evolution"
      }
    ],
    type: states.POKEMONSTATE
  },
  learnmove: {
    name: "learnmove",
    music: [],
    type: states.POKEMONSTATE
  },
  levelup: {
    name: "levelup",
    music: [],
    type: states.POKEMONSTATE
  },
};