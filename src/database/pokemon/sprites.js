import BulbasaurFront from 'assets/images/pokemon/bulbasaur.png';
import BulbasaurBack from 'assets/images/pokemon/bulbasaur-back.png';
import IvysaurFront from 'assets/images/pokemon/ivysaur.png';
import VenusaurFront from 'assets/images/pokemon/venusaur.png';

import CharmanderFront from 'assets/images/pokemon/charmander.png';
import CharmanderBack from 'assets/images/pokemon/charmander-back.png';
import CharmeleonFront from 'assets/images/pokemon/charmeleon.png';
import CharizardFront from 'assets/images/pokemon/charizard.png';

import SquirtleFront from 'assets/images/pokemon/squirtle.png';
import SquirtleBack from 'assets/images/pokemon/squirtle-back.png';
import WartortleFront from 'assets/images/pokemon/wartortle.png';
import BlastoiseFront from 'assets/images/pokemon/blastoise.png';

import EeveeFront from 'assets/images/pokemon/eevee.png';
import EeveeBack from 'assets/images/pokemon/eevee-back.png';

export const sprites = {
  bulbasaur: {
    front: BulbasaurFront,
    back: BulbasaurBack
  },
  ivysaur: {
    front: IvysaurFront
  },
  venusaur: {
    front: VenusaurFront
  },
  charmander: {
    front: CharmanderFront,
    back: CharmanderBack
  },
  charmeleon: {
    front: CharmeleonFront
  },
  charizard: {
    front: CharizardFront
  },
  squirtle: {
    front: SquirtleFront,
    back: SquirtleBack
  },
  wartortle: {
    front: WartortleFront
  },
  blastoise: {
    front: BlastoiseFront
  },
  eevee: {
    front: EeveeFront,
    back: EeveeBack
  },
}