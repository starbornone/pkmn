export const abilities = {
  blaze: {
    name: "Blaze",
    description: "Powers up Fire-type moves when the Pokémon's HP is low."
  },
  overgrow: {
    name: "Overgrow",
    description: "Powers up Grass-type moves when the Pokémon's HP is low."
  },
  torrent: {
    name: "Torrent",
    description: "Powers up Water-type moves when the Pokémon's HP is low."
  }
}