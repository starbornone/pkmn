export const stats = [
  {
    name: 'HP',
    keyValue: 'hp'
  },
  {
    name: 'Attack',
    keyValue: 'attack'
  },
  {
    name: 'Defense',
    keyValue: 'defense'
  },
  {
    name: 'Sp. Attack',
    keyValue: 'spatk'
  },
  {
    name: 'Sp. Defense',
    keyValue: 'spdef'
  },
  {
    name: 'Speed',
    keyValue: 'speed'
  }
]