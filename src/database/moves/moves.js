import { categories, effects, targets, types } from 'database';
import { status } from 'database/pokemon/status';

export const moves = {
  ember: {
    name: "Ember",
    type: types.fire,
    category: categories.special,
    description: "The target is attacked with small flames. It may also leave the target with a burn.",
    effects: [
      {
        type: effects.DAMAGE,
        targets: targets.OPPONENT_SINGLE
      },
      // {
      //   type: effects.APPLIES_STATUS,
      //   chance: "0.1",
      //   status: status.burn,
      //   targets: targets.OPPONENT_SINGLE
      // }
    ],
    power: 40,
    accuracy: 100,
    pp: 25
  },
  growl: {
    name: "Growl",
    type: types.normal,
    category: categories.status,
    description: "The user growls in a cute way, making the foe lower its Attack stat.",
    power: 0,
    accuracy: 100,
    pp: 40
  },
  growth: {
    name: "Growth",
    type: types.normal,
    category: categories.status,
    description: "The user's body is forced to grow, raising the Special Attack stat.",
    power: 0,
    accuracy: 0,
    pp: 20
  },
  leechSeed: {
    name: "Leech Seed",
    type: types.grass,
    category: categories.status,
    description: "A seed is planted on the target. It steals some HP from the target every turn.",
    power: 0,
    accuracy: 90,
    pp: 10
  },
  rapidSpin: {
    name: "Rapid Spin",
    type: types.normal,
    category: categories.physical,
    description: "A spin attack that can also eliminate such moves as Bind, Wrap, Leech Seed, and Spikes.",
    effects: [
      {
        type: effects.DAMAGE,
        targets: targets.OPPONENT_SINGLE
      }
    ],
    power: 50,
    accuracy: 100,
    pp: 40
  },
  scratch: {
    name: "Scratch",
    type: types.normal,
    category: categories.physical,
    description: "Hard, pointed, sharp claws rake the target to inflict damage.",
    effects: [
      {
        type: effects.DAMAGE,
        targets: targets.OPPONENT_SINGLE
      }
    ],
    power: 40,
    accuracy: 100,
    pp: 35
  },
  smokescreen: {
    name: "Smokescreen",
    type: types.normal,
    category: categories.status,
    description: "The user releases an obscuring cloud of smoke or ink. This lowers the target's accuracy.",
    power: 0,
    accuracy: 100,
    pp: 20
  },
  tackle: {
    name: "Tackle",
    type: types.normal,
    category: categories.physical,
    description: "A physical attack in which the user charges and slams into the target with its whole body.",
    effects: [
      {
        type: effects.DAMAGE,
        targets: targets.OPPONENT_SINGLE
      }
    ],
    power: 40,
    accuracy: 100,
    pp: 35
  },
  tailWhip: {
    name: "Tail Whip",
    type: types.normal,
    category: categories.status,
    description: "The user wags its tail cutely, making opposing Pokémon less wary and lowering their Defense stats.",
    power: 0,
    accuracy: 100,
    pp: 30
  },
  vineWhip: {
    name: "Vine Whip",
    type: types.grass,
    category: categories.physical,
    description: "The target is struck with slender, whiplike vines to inflict damage.",
    effects: [
      {
        type: effects.DAMAGE,
        targets: targets.OPPONENT_SINGLE
      }
    ],
    power: 45,
    accuracy: 100,
    pp: 25
  },
  waterGun: {
    name: "Water Gun",
    type: types.water,
    category: categories.special,
    description: "The target is blasted with a forceful shot of water.",
    effects: [
      {
        type: effects.DAMAGE,
        targets: targets.OPPONENT_SINGLE
      }
    ],
    power: 40,
    accuracy: 100,
    pp: 25
  },
  withdraw: {
    name: "Withdraw",
    type: types.water,
    category: categories.status,
    description: "The user withdraws its body into its hard shell, raising its Defense stat.",
    power: 0,
    accuracy: 0,
    pp: 40
  }
}