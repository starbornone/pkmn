export const categories = {
  physical: {
    name: 'Physical'
  },
  special: {
    name: 'Special'
  },
  status: {
    name: 'Status'
  }
}