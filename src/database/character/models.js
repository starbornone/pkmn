import FemaleFront from 'assets/images/character/hilda.png'
import FemaleBack from 'assets/images/character/hilda-back.png'
import FemaleClose from 'assets/images/character/hilda-close.png'

import MaleFront from 'assets/images/character/hilbert.png'
import MaleBack from 'assets/images/character/hilbert-back.png'
import MaleClose from 'assets/images/character/hilbert-close.png'

export const characterModels = [
  {
    name: "Female",
    fieldValue: "female",
    sprite: {
      main: {
        url: FemaleFront,
        height: "80px",
        width: "80px"
      },
      back: {
        url: FemaleBack,
        height: "120px",
        width: "73px"
      },
      close: {
        url: FemaleClose,
        height: "64px",
        width: "128px"
      },
    }
  }, {
    name: "Male",
    fieldValue: "male",
    sprite: {
      main: {
        url: MaleFront,
        height: "80px",
        width: "80px"
      },
      back: {
        url: MaleBack,
        height: "120px",
        width: "73px"
      },
      close: {
        url: MaleClose,
        height: "64px",
        width: "128px"
      },
    }
  }
]

