import PalletTownMap from 'assets/images/maps/pallet-town.png';
import PalletTownTheme from 'assets/audio/pallet-town.mp3';

import { states } from 'database';

export const locationStates = {
  pallettown: {
    name: "pallettown",
    map: {
      url: PalletTownMap
    },
    music: [
      {
        musicSrc: PalletTownTheme,
        name: 'Pallet Town Theme'
      }
    ],
    type: states.LOCATIONSTATE
  }
} 