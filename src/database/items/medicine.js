import Potion from 'assets/images/items/medicine/potion.png';

export const medicine = {
  potion: {
    canBeHeld: true,
    canUseInBattle: true,
    canUseOutsideBattle: true,
    consumedOnUse: true,
    cost: 200,
    description: "A spray-type medicine for treating wounds. It can be used to restore 20 HP to an injured Pokémon.",
    name: "Potion",
    sprite: Potion,
    stackable: true
  }
}