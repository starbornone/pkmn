import FireStone from 'assets/images/items/evo-item/fire-stone.png';
import IceStone from 'assets/images/items/evo-item/ice-stone.png';
import LeafStone from 'assets/images/items/evo-item/leaf-stone.png';
import ThunderStone from 'assets/images/items/evo-item/thunder-stone.png';
import WaterStone from 'assets/images/items/evo-item/water-stone.png';

export const miscItems = {
  firestone: {
    canBeHeld: true,
    canUseInBattle: false,
    canUseOutsideBattle: true,
    consumedOnUse: true,
    cost: 2100,
    description: "A peculiar stone that can make certain species of Pokémon evolve. The stone has a fiery orange heart.",
    name: "Fire Stone",
    sprite: FireStone,
    stackable: true
  },
  icestone: {
    canBeHeld: true,
    canUseInBattle: false,
    canUseOutsideBattle: true,
    consumedOnUse: true,
    cost: 2100,
    description: "A peculiar stone that can make certain species of Pokémon evolve. It has an unmistakable snowflake pattern.",
    name: "Ice Stone",
    sprite: IceStone,
    stackable: true
  },
  leafstone: {
    canBeHeld: true,
    canUseInBattle: false,
    canUseOutsideBattle: true,
    consumedOnUse: true,
    cost: 2100,
    description: "A peculiar stone that can make certain species of Pokémon evolve. It has an unmistakable leaf pattern.",
    name: "Leaf Stone",
    sprite: LeafStone,
    stackable: true
  },
  thunderstone: {
    canBeHeld: true,
    canUseInBattle: false,
    canUseOutsideBattle: true,
    consumedOnUse: true,
    cost: 2100,
    description: "A peculiar stone that can make certain species of Pokémon evolve. It has a distinct thunderbolt pattern.",
    name: "Thunder Stone",
    sprite: ThunderStone,
    stackable: true
  },
  waterstone: {
    canBeHeld: true,
    canUseInBattle: false,
    canUseOutsideBattle: true,
    consumedOnUse: true,
    cost: 2100,
    description: "A peculiar stone that can make certain species of Pokémon evolve. It is the blue of a pool of clear water.",
    name: "Water Stone",
    sprite: WaterStone,
    stackable: true
  },
}