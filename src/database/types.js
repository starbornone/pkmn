export const types = {
  bug: {
    name: "Bug",
    colour: "#f3d06a"
  },
  electric: {
    name: "Electric",
    colour: "#f3d06a"
  },
  fire: {
    name: "Fire",
    colour: "#ea5357"
  },
  grass: {
    name: "Grass",
    colour: "#8cc661"
  },
  normal: {
    name: "Normal",
    colour: "#c3d6e0"
  },
  poison: {
    name: "Poison",
    colour: "#a661a4"
  },
  water: {
    name: "Water",
    colour: "#6289c6"
  }
}

export const typeStrengths = {
  bug: [],
  electric: [],
  fire: [
    types.bug,
    types.grass
  ],
  grass: [
    types.water
  ],
  normal: [],
  poison: [],
  water: [
    types.fire
  ]
}

export const typeWeaknesses = {
  bug: [
    types.fire
  ],
  electric: [],
  fire: [
    types.water
  ],
  grass: [
    types.fire
  ],
  normal: [],
  poison: [],
  water: [
    types.grass
  ]
}