export const time = {
  day: {
    name: "Day",
    hours: {
      start: "0600",
      end: "1959"
    }
  },
  night: {
    name: "Night",
    hours: {
      start: "2000",
      end: "0559"
    }
  },
}