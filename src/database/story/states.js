import OpeningMovie from 'assets/audio/opening-movie.mp3';
import ProfessorOakLaboratory from 'assets/audio/professor-oak-laboratory.mp3';
import TitleScreen from 'assets/audio/title-screen.mp3';
import WelcomeToTheWorldOfPokemon from 'assets/audio/welcome-to-the-world-of-pokemon.mp3';

import { states } from 'database';

export const storyStates = {
  charactercreate: {
    name: "charactercreate",
    music: [
      {
        musicSrc: WelcomeToTheWorldOfPokemon,
        name: "Welcome to the World of Pokemon"
      }
    ],
    type: states.STORYSTATE
  },
  starterintro: {
    name: "starterintro",
    music: [
      {
        musicSrc: WelcomeToTheWorldOfPokemon,
        name: "Welcome to the World of Pokemon"
      }
    ],
    type: states.STORYSTATE
  },
  starterselect: {
    name: "starterselect",
    music: [
      {
        musicSrc: ProfessorOakLaboratory,
        name: "Professor Oak's Laboratory"
      }
    ],
    type: states.STORYSTATE
  },
  titlescreen: {
    name: "titlescreen",
    music: [
      {
        musicSrc: OpeningMovie,
        name: "Opening Movie"
      },
      {
        musicSrc: TitleScreen,
        name: "Title Screen"
      }
    ],
    type: states.STORYSTATE
  }
}