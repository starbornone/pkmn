import React, { memo } from 'react';

import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { states } from 'database';

function Item(props) {
  const { currentState, item } = props;

  let canUse;
  if (currentState.type === states.BATTLESTATE) {
    canUse = item.canUseInBattle;
  } else {
    canUse = item.canUseOutsideBattle;
  }

  return <Paper className="p-4 mb-2" variant="outlined">
    <Grid
      container
      direction="row"
      justify="flex-start"
      spacing={0}
    >
      <Grid item xs={3}>
        <img
          className="bg-gray-200 rounded-full"
          src={item.sprite}
        />
        {canUse ? <div className="mt-1 mx-1">
          <IconButton size="small">
            <i className="fal fa-long-arrow-alt-left"></i>
          </IconButton>
        </div> : null}
        {canUse ? <div className="mt-1 mx-1">
          <IconButton size="small">
            <i className="fal fa-random fa-flip-horizontal"></i>
          </IconButton>
        </div> : null}
      </Grid>
      <Grid item xs={9}>
        <Typography variant="subtitle2">{item.name}</Typography>
        <Typography variant="caption">{item.description}</Typography>
      </Grid>
    </Grid>
  </Paper>
}

export default memo(Item);