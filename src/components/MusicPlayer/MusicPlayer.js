import React from 'react';
import { useSelector } from 'react-redux';

import ReactJkMusicPlayer from 'react-jinke-music-player';
import 'react-jinke-music-player/assets/index.css';

function MusicPlayer(props) {
  const currentState = useSelector(({ story }) => story.currentState);

  return <ReactJkMusicPlayer
    audioLists={currentState.music}
    autoHiddenCover
    autoPlay={false}
    defaultPlayMode="orderLoop"
    mode="full"
    showDownload={false}
    showMediaSession
    showPlayMode={false}
    showReload={false}
    showThemeSwitch={false}
    theme="light"
  />
}

export default MusicPlayer;