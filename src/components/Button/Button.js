import _ from '@lodash';
import React, { memo } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { themeColours } from 'assets/themeColours';

function StyledButton(props) {
  const { children, color, variant } = props;

  if (!color) {
    return <Button {...props}>{children}</Button>
  }

  const palette = themeColours.palette[color];

  const ColorButton = withStyles((theme) => ({
    root: {
      color: variant === 'contained' ? theme.palette.getContrastText(palette.main) : palette.dark,
      backgroundColor: variant === 'contained' && palette.main,
      borderColor: variant === 'outlined' ? palette.main : 'none',
      '&:hover': {
        backgroundColor: variant === 'contained' && palette.dark,
        borderColor: variant === 'outlined' ? palette.dark : 'none',
      },
    },
  }))(Button);

  return <ColorButton {...props}>{children}</ColorButton >
}

export default memo(StyledButton);