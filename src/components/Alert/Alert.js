import React, { memo } from 'react';

function Alert(props) {
  const { children } = props;

  return <div className="text-center py-4 lg:px-4">
    <div className="p-2 bg-grey-800 items-center text-white leading-none lg:rounded-full flex lg:inline-flex" role="alert">
      <span className="flex rounded-full bg-grey-500 uppercase px-2 py-1 text-xs font-semibold mr-3">New</span>
      <span className="mr-2 text-left flex-auto">{children}</span>
    </div>
  </div>
}

export default memo(Alert);