import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 3,
  },
  colorPrimary: {
    backgroundColor: '#b8dbb3',
  },
  bar: {
    backgroundColor: '#61bc55',
  },
}))(LinearProgress);

function LinearProgressWithLabel(props) {
  return <Box display="flex" alignItems="center">
    <Box width="100%" mr={1}>
      <BorderLinearProgress variant="determinate" value={props.value} />
    </Box>
    <Box minWidth={40}>
      <Typography variant="caption" color="textSecondary">{props.currentHP} / {props.maxHP}</Typography>
    </Box>
  </Box>
}

function HealthBar(props) {
  const { health } = props;

  return <div className="w-full">
    <LinearProgressWithLabel
      currentHP={health.currentHP}
      maxHP={health.maxHP}
      variant="determinate"
      value={(health.currentHP / health.maxHP) * 100}
    />
  </div>
}

export default HealthBar;