import Home from 'main/pages/Home/Home';
import NewGame from 'main/pages/NewGame/NewGame';
import Game from 'main/pages/Game/Game';

export const routes = [
  {
    component: Home,
    exact: true,
    id: 'home-01',
    path: '/'
  }, {
    component: NewGame,
    exact: true,
    id: 'new-game-01',
    path: '/new-game'
  }, {
    component: Game,
    exact: true,
    id: 'game-01',
    path: '/game'
  }
]